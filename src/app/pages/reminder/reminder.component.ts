/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, AfterViewInit, ViewChildren, QueryList, OnInit, EventEmitter, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { User } from '../../model/user';
import { Notification } from '../../model/notification';
import { StatusEn, BannerMessageStatusEn } from '../../common/enums';
import { BannerMessage } from '../../common/banner-message';
import { OverviewComponent } from '../../pages/overview/overview.component';
import { Observable } from 'rxjs/Observable';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { ResponsibilityService } from '../../services/responsibility.service';
import { FilterComponent } from '../../filter/filter.component';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/timer';
import { slideInOpacity } from '../../common/router.animations';

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.css'],
  animations: [ slideInOpacity()]
})
export class ReminderComponent implements OnInit {

  @HostBinding('@slideInOpacity') get slideInOpacity(){
    return '';
  }
  private dialogConfig = new MdDialogConfig();
  private bannerMessageStatus = BannerMessageStatusEn;
  public user: User = null;
  responsiblitiesRetrieveDone = false;
  public bannerMessage: BannerMessage = new BannerMessage();
  responsibilitiesContainer: TerritoryResponsibility[];
  shiftChangeClosed = false;
  shiftChangeOpened = false;
  filterExpanded = false;

  constructor(
    private router: Router,
    public dialog: MdDialog,
    public sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService
  ) { }

  ngOnInit() {
    this.dialogConfig.disableClose = true;
    this.user = this.sessionContext.getCurrUser();
    this.filterExpanded = this.sessionContext.getFilterExpansionState();
  }

  openDialogEditEntry(notification: Notification) {
    const notificationObj = Object.assign(new Notification(), notification);
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isInstructionDialog = notification.adminFlag;
    dialogRef.componentInstance.isPresenceReminderDialog = notificationObj.isTypePresenceCheck();
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isEditDialog = true;
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogLookUpEntry(notification: Notification) {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isReadOnlyDialog = true;
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
