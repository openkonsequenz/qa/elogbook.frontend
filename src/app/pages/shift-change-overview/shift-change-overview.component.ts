/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { SessionContext } from '../../common/session-context';
import { ResponsibilityService } from '../../services/responsibility.service';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { slideInOpacity } from '../../common/router.animations';
import { BannerMessageStatusEn, ErrorType } from '../../common/enums';

import { MessageService, MessageDefines } from '../../services/message.service';

@Component({
  selector: 'app-shift-change-overview',
  templateUrl: './shift-change-overview.component.html',
  styleUrls: ['./shift-change-overview.component.css'],  
  animations: [ slideInOpacity()]
})

export class ShiftChangeOverviewComponent implements OnInit {

  @HostBinding('@slideInOpacity') get slideInOpacity(){
    return '';
  }
  responsibilitiesContainer: TerritoryResponsibility[];
  shiftChangeTransactionId: number;

  shiftChangeDateSelected(scTransId: number) {
    this.shiftChangeTransactionId = scTransId;
    this.retrieveHistoricalResponsibilities(scTransId);
  }

  constructor(
    private messageService: MessageService,
    private router: Router,
    public sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService) { }

  ngOnInit() {
  }

  retrieveHistoricalResponsibilities(transactionId: number): void {
    this.responsibilityService.getHistoricalResponsibilities(transactionId).subscribe(resp => {
      this.responsibilitiesContainer = resp;
    },
      error => {
        this.messageService.emitError('Verantwortlichkeiten', ErrorType.retrieve);
      });
  }

  gotToOverview() {
    this.router.navigate(['/overview']);
  }

}
