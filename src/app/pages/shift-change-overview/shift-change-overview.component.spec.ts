/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { click } from '../../testing/index';
import { NgModule } from '@angular/core';
import { DebugElement, EventEmitter, SimpleChange } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MdDialogModule, MaterialModule, MdDialog, MdDialogConfig, Overlay, OverlayContainer, OVERLAY_PROVIDERS } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { MockComponent } from '../../testing/mock.component';
import { Router } from '@angular/router';
import { User } from '../../model/user';
import { SessionContext } from '../../common/session-context';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { NotificationService } from '../../services/notification.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { MessageService } from '../../services/message.service';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { EntryComponentMocker } from '../../dialogs/entry/entry.component.spec';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { ShiftChangeOverviewComponent } from './shift-change-overview.component';
import { RESPONSIBILITIES_SHIFT_CHANGE } from '../../test-data/responsibilities';
import { HISTORICAL_SCHIFTCHANGES } from '../../test-data/historical-shiftchanges';
import { USERS } from '../../test-data/users';
import { FutureNotificationsMocker } from '../../lists/future-notifications/future-notifications.component.spec';
import { OpenNotificationsMocker } from '../../lists/open-notifications/open-notifications.component.spec';
import { FinishedNotificationsMocker } from '../../lists/finished-notifications/finished-notifications.component.spec';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { AbstractListMocker } from '../../lists/abstract-list/abstract-list.component.spec';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}
@NgModule({
  declarations: [EntryComponent],
  entryComponents: [
    EntryComponent
  ]
})
class TestModule { }

describe('ShiftChangeOverviewComponent', () => {
  let component: ShiftChangeOverviewComponent;
  let fixture: ComponentFixture<ShiftChangeOverviewComponent>;
  let sessionContext: SessionContext;
  let router: Router;
  let mockNotificationService: MockNotificationService;
  //let mockReminderService: MockReminderService;
  let mockRespService: MockResponsibilityService;
  let messageService;
  let routerStub: FakeRouter;
  
    routerStub = {
      navigate: jasmine.createSpy('navigate').and.callThrough()
    };

  class MockResponsibilityService extends AbstractMockObservableService {
    plannedResponsibilities = null;
    responsibilities = null;
    historicalResponsibilities = HISTORICAL_SCHIFTCHANGES;

    getPlannedResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.plannedResponsibilities;
      return resptService;
    };
    getResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.responsibilities;
      return resptService;
    }
    getHistoricalResponsibilities(transactionId: number) {
      const resptService = new MockResponsibilityService();
      resptService.content = this.historicalResponsibilities;
      return this; 
    }
  }

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getOpenNotifications(notificationType: string) {
      this.loadCalled = true;
      return this;
    };
    public getFutureNotifications(notificationType: string) {
      this.loadCalled = true;
      return this;
    };
    public getFinishedNotifications(notificationType: string) {
      this.loadCalled = true;
      return this;
    };
  }

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    messageService = new MessageService();
    mockRespService = new MockResponsibilityService();    
    //mockReminderService = new MockReminderService();
    mockNotificationService = new MockNotificationService();
    //mockAuthService = new MockAuthService();
    router = new FakeRouter() as any as Router;

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [EntryComponent]
      }
    });

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MdDialogModule,
        BrowserAnimationsModule,
        MaterialModule.forRoot()],
      declarations: [ 
        ShiftChangeOverviewComponent,
        EntryComponent,
        FormattedTimestampPipe,
        StringToDatePipe,
        MockComponent({ selector: 'app-loading-spinner' }),

        MockComponent({ selector: 'app-historical-shift-changes' }),
        MockComponent({
          selector: 'app-responsibility', 
          inputs: ['withButtons', 'withNames', 'responsiblitySelection']
        }),
        AbstractListMocker.getComponentMocks(),
        MockComponent({ selector: 'input', inputs: ['options'] }),           
        FinishedNotificationsMocker.getComponentMocks(),
        OpenNotificationsMocker.getComponentMocks(true),
        FutureNotificationsMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-message-banner'
        }),
        EntryComponentMocker.getComponentMocks()
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: SessionContext, useValue: sessionContext },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: Overlay, useClass: Overlay },
        { provide: OVERLAY_PROVIDERS, useClass: OVERLAY_PROVIDERS },
        
        { provide: ResponsibilityService, useValue: mockRespService },
        { provide: MessageService, useValue: messageService },
        { provide: NotificationService, useValue: mockNotificationService }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShiftChangeOverviewComponent);
    component = fixture.componentInstance;
    
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftChangeOverviewComponent);
    component = fixture.componentInstance;
    
  });

  it('should be created', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should navigate to overview on home-button click', () => {
    component.gotToOverview();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
  });

  it('should call getHistoricalResponsibilities service method and return an error', async(() => {
    let hasBeenCalled = false;
    sessionContext.setCurrUser(USERS[1]);
   
    messageService.errorOccured$.subscribe( msg => hasBeenCalled = true);
    const transactionId = -1;
    mockRespService.error = 'RETRIEVEHISTORICALRESPONSIBILITIES_ERROR';
    mockRespService.content = HISTORICAL_SCHIFTCHANGES;

    fixture.detectChanges();
    mockRespService.getHistoricalResponsibilities(transactionId).subscribe((resp) => {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.sessionContext.bannerMessage.text).toEqual('Verantwortlichkeiten');
        expect(hasBeenCalled).toBeTruthy();
      });
    });

    component.shiftChangeDateSelected(transactionId);
    fixture.detectChanges();
  }));

  it('should call service after ShiftChange is selected ', async(() => {
    component.responsibilitiesContainer = null;  
    mockRespService.content = HISTORICAL_SCHIFTCHANGES;

    component.shiftChangeDateSelected(HISTORICAL_SCHIFTCHANGES.historicalResponsibilities[0].transactionId);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.responsibilitiesContainer).toBe(HISTORICAL_SCHIFTCHANGES);
    });
  }));
});
