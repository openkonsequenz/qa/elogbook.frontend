/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { FormsModule } from '@angular/forms';
import { async, fakeAsync, ComponentFixture, TestBed, tick, inject } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Injectable, DebugElement, EventEmitter, SimpleChange } from '@angular/core';
import { click, newEvent } from '../../testing/index';
import { MdDialogRef } from '@angular/material';
import { MdDialogModule, MaterialModule, MdDialog, MdDialogConfig, Overlay, OverlayContainer, OVERLAY_PROVIDERS } from '@angular/material';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { MockComponent } from '../../testing/mock.component';
import { AppModule } from '../../app.module';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';
import { SessionContext } from '../../common/session-context';
import { Router } from '@angular/router';
import { User } from '../../model/user';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { SearchComponent } from './search.component';
import { SearchResultListComponent } from '../../lists/search-result-list/search-result-list.component';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../services/notification.service';
import { SearchResultService } from '../../services/search-result.service';
import { SEARCH_RESULT_NOTIFICATIONS } from '../../test-data/search-result-notifications';
import { FilterSelection } from '../../model/filter-selection';
import { CurrentRemindersComponent } from '../../lists/current-reminders/current-reminders.component';
import { ReminderComponent } from '../reminder/reminder.component';
import { ReminderSearchFilter } from '../../model/reminder-search-filter';
import { ReminderService } from '../../services/reminder.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessageService } from '../../services/message.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { ShiftChangeProtocolComponent } from '../../dialogs/shift-change-protocol/shift-change-protocol.component';
import { USERS } from '../../test-data/users';
import { FutureNotificationsMocker } from '../../lists/future-notifications/future-notifications.component.spec';
import { OpenNotificationsMocker } from '../../lists/open-notifications/open-notifications.component.spec';
import { FinishedNotificationsMocker } from '../../lists/finished-notifications/finished-notifications.component.spec';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { AbstractListMocker } from '../../lists/abstract-list/abstract-list.component.spec';
import { UserService } from 'app/services/user.service';
import { USERSETTING_1 } from 'app/test-data/user-settings';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}
@NgModule({
  declarations: [EntryComponent, ShiftChangeProtocolComponent],
  entryComponents: [
    EntryComponent, ShiftChangeProtocolComponent
  ]
})
class TestModule { }

describe('SearchComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let router: Router;
  let mockNotificationService: MockNotificationService;
  let mockNotificationSearchSortService: MockNotificationSearchSortService;
  let mockSearchResultService: MockSearchResultService;
  let messageService;
  let routerStub: FakeRouter;
  let mockUserService;

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  const correctUser: User = {
    id: '44', username: 'carlo', password: 'serverPwd'
    , name: 'Carlo Cottura', specialUser: true, itemName: this.userName
  };

  class MockResponsibilityService extends AbstractMockObservableService {
    plannedResponsibilities = null;
    responsibilities = null;

    getPlannedResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.plannedResponsibilities;
      return resptService;
    };
    getResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.responsibilities;
      return resptService;
    }
  }

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;

    getNotificationVersions(incidentId: number) {
      return this;
    }
  }

  class MockNotificationSearchSortService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    searchResultNotifications = SEARCH_RESULT_NOTIFICATIONS;
  }

  class MockSearchResultService extends AbstractMockObservableService {
    public getSearchResults(searchResultFilter?: GlobalSearchFilter) {

      const searchResultNotifications = SEARCH_RESULT_NOTIFICATIONS;
      this.content = searchResultNotifications;
      return this;
    }
  }

  class MockDialogRef extends AbstractMockObservableService {
    close() { }
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    };

    getUserSettings() {
      return this;
    };

    postUserSettings() {
      return this;
    };    
  } 

  beforeEach(async(() => {
    messageService = new MessageService();
    mockNotificationService = new MockNotificationService();
    mockNotificationSearchSortService = new MockNotificationSearchSortService();
    mockSearchResultService = new MockSearchResultService();
    mockUserService = new MockUserService();
    router = new FakeRouter() as any as Router;

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [EntryComponent, ShiftChangeProtocolComponent]
      }
    });

    TestBed.configureTestingModule({
      imports: [FormsModule,
        BrowserAnimationsModule,
        MdDialogModule,
        MaterialModule.forRoot()],
      declarations: [
        SearchComponent,
        EntryComponent,
        ShiftChangeProtocolComponent,
        MainNavigationComponent,
        SearchResultListComponent,
        FormattedTimestampPipe,
        StringToDatePipe,
        MockComponent({ selector: 'app-entry' }),
        MockComponent({
          selector: 'app-responsibility', inputs: [
            'responsiblitySelection']
        }),
        MockComponent({ selector: 'input', inputs: ['options'] }),
        MockComponent({ selector: 'app-search', inputs: ['withCheckboxes', 'withEditButtons', 'isCollapsible'] }),
        MockComponent({ selector: 'app-autocomplete', inputs: ['notification'], outputs: ['notification'] }),
        MockComponent({ selector: 'app-loading-spinner' }),
        AbstractListMocker.getComponentMocks(),
        FinishedNotificationsMocker.getComponentMocks(),
        OpenNotificationsMocker.getComponentMocks(true),
        FutureNotificationsMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-message-banner'
        }),
        MockComponent({ selector: 'app-remove-button', inputs: ['nullableAttribute'], outputs: ['nullableAttribute'] })        
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: MdDialogRef, useClass: MockDialogRef },
        { provide: Overlay, useClass: Overlay },
        { provide: OVERLAY_PROVIDERS, useClass: OVERLAY_PROVIDERS },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: NotificationService, useValue: mockNotificationService },
        { provide: SearchResultService, useValue: mockSearchResultService },
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: ReminderService, useClass: ReminderService },
        { provide: ResponsibilityService, useClass: MockResponsibilityService },
        { provide: MessageService, useValue: messageService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    sessionContext.clearStorage();
    sessionContext.setCurrUser(USERS[0]);
    component.sessionContext = sessionContext;
  });

  it('should init correctly with correct user', async(() => {
    sessionContext.setCurrUser(correctUser);

    mockNotificationSearchSortService.searchResultNotifications = [];
    fixture.detectChanges();
    expect(component.user.id).toBe(correctUser.id);
  }));

  it('should should navigate to Overview after click at homebbutton', (() => {
    spyOn(component, 'goToOverview').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const button = fixture.debugElement.nativeElement.querySelector('button#homebutton');
      button.click();
      fixture.detectChanges();
      expect(component.goToOverview).toHaveBeenCalled();
      expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
    });
  }));

  it('should navigate to overview on home-button click', () => {
    component.goToOverview();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
  });

  it('should open a dialog to view a notification on button click', async(() => {

    let entryOpend = false;
    let dialog;
    const testNotification = SEARCH_RESULT_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogLookUpEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(false);

    });
  }));


  it('should open an dialog to edit a notification on button click', async(() => {

    let entryOpend = false;
    let dialog;
    const testNotification = SEARCH_RESULT_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogEditEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(true);

    });
  }));

  it('should delete search parameters after Suche löschen button pressed', (() => {
    let dialog;
    const globalSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
    globalSearchFilter.searchString = 'Ab1224ag';
    globalSearchFilter.responsibilityForwarding = 'Karl Heinz';
    globalSearchFilter.statusOpenSelection = false;
    globalSearchFilter.statusInWorkSelection = false;
    globalSearchFilter.statusDoneSelection = false;
    globalSearchFilter.statusClosedSelection = false;
    globalSearchFilter.fkRefBranch = 2;
    globalSearchFilter.fkRefGridTerritory = 1;
    globalSearchFilter.fastSearchSelected = false;

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      spyOn(dialog, 'deleteSearch');

    });

    fixture.whenStable().then(() => {
      const button = fixture.debugElement.nativeElement.querySelector('button#deletesearch');
      button.click();
      fixture.detectChanges();

      expect(component.globalSearchFilter.searchString).toBe('');
      expect(component.globalSearchFilter.responsibilityForwarding).toBe('');

      expect(component.globalSearchFilter.statusOpenSelection).toBe(true);
      expect(component.globalSearchFilter.statusInWorkSelection).toBe(true);
      expect(component.globalSearchFilter.statusDoneSelection).toBe(true);
      expect(component.globalSearchFilter.statusClosedSelection).toBe(true);
      expect(component.globalSearchFilter.fkRefBranch).toBe(-1);
      expect(component.globalSearchFilter.fkRefGridTerritory).toBe(-1);
      expect(component.globalSearchFilter.fastSearchSelected).toBe(true);
    });
  }));


  it('should execute fast search and store it in the session context', (() => {
    let dialog;
    const globalSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
    globalSearchFilter.searchString = 'a';
    globalSearchFilter.statusOpenSelection = true;
    globalSearchFilter.fastSearchSelected = true;

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      spyOn(dialog, 'search');

    });

    fixture.whenStable().then(() => {
      const button = fixture.debugElement.nativeElement.querySelector('button#search');
      button.click();
      fixture.detectChanges();
      expect(component.currentSearchFilter).toBe(true);
      expect(sessionContext.getGlobalSearchFilter().fastSearchSelected).toBe(true);
    });
  }));

  it('should call search function after search button pressed', async(() => {
    spyOn(component, 'search').and.callThrough();
    sessionContext.setUsersSettings(USERSETTING_1);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const currentSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
      currentSearchFilter.searchString = 'Test';
      currentSearchFilter.responsibilityForwarding = 'Max';
      currentSearchFilter.statusOpenSelection = false;
      currentSearchFilter.statusInWorkSelection = false;
      currentSearchFilter.statusDoneSelection = false;
      currentSearchFilter.statusClosedSelection = false;
      currentSearchFilter.fkRefBranch = 1;
      currentSearchFilter.fkRefGridTerritory = 2;
      currentSearchFilter.fastSearchSelected = false;
      component.currentSearchFilter = currentSearchFilter;
      let de: DebugElement;

      de = fixture.debugElement.queryAll(By.css('form button:not(#deletesearch)'))[1];
      click(de);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.search).toHaveBeenCalled();
        expect(component.sessionContext.getGlobalSearchFilter()).not.toBeNull();
        expect(component.sessionContext.getGlobalSearchFilter().searchString).toBe(currentSearchFilter.searchString);

      });
    });
  }));
});
