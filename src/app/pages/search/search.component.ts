/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, AfterViewInit, ViewChildren, QueryList, OnInit, EventEmitter, Optional, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { MdDialogRef } from '@angular/material';
import { User } from '../../model/user';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { StatusSelectionObject } from '../../model/status-selection-object';
import { Notification } from '../../model/notification';
import { Status } from '../../model/status';
import { Branch } from '../../model/branch';
import { GridTerritory } from '../../model/gridterritory';
import { StatusEn, BannerMessageStatusEn } from '../../common/enums';
import { BannerMessage } from '../../common/banner-message';
import { OverviewComponent } from '../../pages/overview/overview.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { SearchResultService } from '../../services/search-result.service';
import {  slideInOpacity } from '../../common/router.animations';
import { Priority } from 'app/model/priority';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
    animations: [ slideInOpacity()]
})
export class SearchComponent implements OnInit {

    @HostBinding('@slideInOpacity') get slideInOpacity(){
        return '';
    }
    private dialogConfig = new MdDialogConfig();
    private bannerMessageStatus = BannerMessageStatusEn;
    public user: User = null;
    public bannerMessage: BannerMessage = new BannerMessage();
    gridTerritories: GridTerritory[];
    branches: Branch[];
    statuses: Status[];
    priorities: Priority[];
    public globalSearchFilter: GlobalSearchFilter;
    public currentSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();

    constructor(
        private router: Router,
        public dialog: MdDialog,
        public sessionContext: SessionContext,
        private searchResultService: SearchResultService
    ) { }

    ngOnInit() {
        this.dialogConfig.disableClose = true;
        this.user = this.sessionContext.getCurrUser();
        this.gridTerritories = this.sessionContext.getMasterGridTerritories();
        this.branches = this.sessionContext.getBranches();
        this.statuses = this.sessionContext.getStatuses();
        this.priorities = this.sessionContext.getPriorities();
        this.currentSearchFilter = this.sessionContext.getGlobalSearchFilter();
        
    }

    public goToOverview() {
        this.router.navigate(['/overview']);
    }

    openDialogLookUpEntry(notification: Notification) {
        const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
        dialogRef.componentInstance.user = this.user;
        dialogRef.componentInstance.setNotification(notification);
        dialogRef.componentInstance.isReadOnlyDialog = true;
        dialogRef.afterClosed().subscribe(result => {
        });
    }

    openDialogEditEntry(notification: Notification) {
        const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
        const notificationObj = Object.assign(new Notification(), notification);
        dialogRef.componentInstance.user = this.user;
        dialogRef.componentInstance.isInstructionDialog = notification.adminFlag;
        dialogRef.componentInstance.isPresenceReminderDialog = notificationObj.isTypePresenceCheck();
        dialogRef.componentInstance.setNotification(notification);
        dialogRef.componentInstance.isEditDialog = true;
        dialogRef.afterClosed().subscribe(result => {
        });
    }

    search() {
        this.globalSearchFilter = Object.assign({}, this.currentSearchFilter);
        this.sessionContext.setGlobalSearchFilter(this.globalSearchFilter);
    }

    deleteSearch(currentSearchFilter) {
        this.sessionContext.setInitGlobalSearchFilterValues();
        this.currentSearchFilter = this.sessionContext.getGlobalSearchFilter();
        this.globalSearchFilter = this.sessionContext.getGlobalSearchFilter();
        
    }

    private setError(errorMessage: string) {
        this.bannerMessage.isActive = true;
        this.bannerMessage.status = BannerMessageStatusEn.error;
        this.bannerMessage.text = 'Es ist ein Fehler aufgetreten. Bitte kontaktieren Sie den Administrator';
        console.log(errorMessage);
    }
}