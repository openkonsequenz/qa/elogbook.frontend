/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutPageComponent } from './logout.component';
import { MockComponent } from '../../testing/mock.component';
import { Router } from '@angular/router';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('LogoutComponent', () => {
  let router: Router;
  let component: LogoutPageComponent;
  let fixture: ComponentFixture<LogoutPageComponent>;

  beforeEach(async(() => {
    router = new FakeRouter() as any as Router;

    TestBed.configureTestingModule({
      declarations: [ LogoutPageComponent,
        MockComponent({ selector: 'app-version-info' })
       ],
       providers: [
        { provide: Router }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

});
