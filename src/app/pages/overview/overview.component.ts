/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, AfterViewInit, ViewChildren, QueryList, OnInit, EventEmitter, HostBinding } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { LogoutComponent } from '../../dialogs/logout/logout.component';
import { ResponsibilityComponent } from '../../dialogs/responsibility/responsibility.component';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { FileImportComponent } from '../../dialogs/file-import/file-import.component';
import { ShiftChangeComponent } from '../../dialogs/shift-change/shift-change.component';
import { ShiftChangeProtocolComponent } from '../../dialogs/shift-change-protocol/shift-change-protocol.component';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { User } from '../../model/user';
import { Notification } from '../../model/notification';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';
import { FinishedNotificationsComponent } from '../../lists/finished-notifications/finished-notifications.component';
import { FutureNotificationsComponent } from '../../lists/future-notifications/future-notifications.component';
import { OpenNotificationsComponent } from '../../lists/open-notifications/open-notifications.component';
import { ErrorType } from '../../common/enums';
import { StatusEn } from '../../common/enums';
import { BannerMessage } from '../../common/banner-message';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { ResponsibilityService } from '../../services/responsibility.service';
import { FilterComponent } from '../../filter/filter.component';
import { MessageService, MessageDefines } from '../../services/message.service';
import { slideInOpacity } from '../../common/router.animations';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  animations: [slideInOpacity()]
})

export class OverviewComponent implements OnInit {

  @HostBinding('@slideInOpacity') get slideInOpacity() {
    return '';
  }
  private dialogConfig = new MdDialogConfig();

  private notifications = null;


  showReminder = true;
  user: User = null;
  responsiblitiesRetrieveDone = false;


  responsibilitiesContainer: TerritoryResponsibility[];
  shiftChangeClosed = false;
  shiftChangeOpened = false;
  filterExpanded_ = false;

  constructor(
    public dialog: MdDialog,
    public sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService,    
    private messageService: MessageService  ) { }

  ngOnInit() {
    this.dialogConfig.disableClose = true;
    this.user = this.sessionContext.getCurrUser();
    this.filterExpanded_ = this.sessionContext.getFilterExpansionState();
    this.getResponsibilities();
    this.messageService.respChangedForCurrentUser$.subscribe(b => this.getResponsibilities() );
  }

  private getResponsibilities(): void {
    this.responsibilityService.getPlannedResponsibilities().subscribe(resps => {
      this.responsiblitiesRetrieveDone = true;
      if (resps.length > 0) {
        this.responsibilitiesContainer = resps;
        this.openDialogShiftChangeProtocol(resps);
      } else {
        this.responsibilityService.getResponsibilities().subscribe(respsCurrent => {
          this.responsibilitiesContainer = respsCurrent;
        },
          error => this.handleGetRespError(error) );
      }
    }, error => this.handleGetRespError(error) );
  }

  private handleGetRespError(error: any) {
    console.log(error);
    this.messageService.emitError('Verantwortlichkeiten', ErrorType.retrieve);
    this.responsiblitiesRetrieveDone = true;
 }

  openDialogNewEntry() {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.isInstructionDialog = false;
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isEditDialog = false;
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openInstructionDialogNewEntry() {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.isInstructionDialog = true;
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isEditDialog = false;
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openPresenceReminderDialogNewEntry() {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.isPresenceReminderDialog = true;
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isEditDialog = false;
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogEditEntry(notification: Notification) {
    const notificationObj = Object.assign(new Notification(), notification);
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isInstructionDialog = notification.adminFlag;
    dialogRef.componentInstance.isPresenceReminderDialog = notificationObj.isTypePresenceCheck();
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isEditDialog = true;
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogLookUpEntry(notification: Notification) {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isReadOnlyDialog = true;
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogShiftChange() {
    const dialogRef = this.dialog.open(ShiftChangeComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.isChangingShift) {
        this.openDialogLogout();
      }
    });
  }

  openDialogShiftChangeProtocol(plannedRespContainer: TerritoryResponsibility[]) {
    const dialogRef = this.dialog.open(ShiftChangeProtocolComponent, this.dialogConfig);
    this.shiftChangeOpened = true;
    dialogRef.componentInstance.setResponsibilitiesContainer(plannedRespContainer);
    dialogRef.afterClosed().subscribe(result => {
      this.shiftChangeClosed = true;
      this.shiftChangeOpened = false;
    });
  }

  openDialogDataImport() {

    const dialogRef = this.dialog.open(FileImportComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.isImportFileSelected) {
        const importNotification = new Notification();

        const branchTmp = this.sessionContext.getBranches().filter(
          (b) => b.name === dialogRef.componentInstance.importFileModel.branchName)[0];
        if (branchTmp) {
          importNotification.fkRefBranch = branchTmp.id;
        }

        const gridTerritoryTmp = this.sessionContext.getGridTerritories().filter(
          (gt) => gt.name === dialogRef.componentInstance.importFileModel.gridTerritoryName)[0];
        if (gridTerritoryTmp) {
          importNotification.fkRefGridTerritory = gridTerritoryTmp.id;
        }

        importNotification.notificationText = dialogRef.componentInstance.importFileModel.notificationText;
        //importNotification.fkRefBranch = dialogRef.componentInstance.importFileModel.branchName
        importNotification.fkRefNotificationStatus = StatusEn.open;

        const dialogRefEntry = this.dialog.open(EntryComponent, this.dialogConfig);
        dialogRefEntry.componentInstance.isInstructionDialog = false;
        dialogRefEntry.componentInstance.user = this.user;
        dialogRefEntry.componentInstance.isEditDialog = false;

        dialogRefEntry.componentInstance.notification = importNotification;
        dialogRefEntry.afterClosed().subscribe(resultRefEntry => {
        });
      }
    });

  }

  openDialogLogout() {
    this.dialogConfig.data = this;
    const dialogRef = this.dialog.open(LogoutComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  isSpecialUser(): boolean {
    return this.user && this.user.specialUser;
  }


}
