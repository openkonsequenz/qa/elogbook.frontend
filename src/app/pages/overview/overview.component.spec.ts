/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { click } from '../../testing/index';
import { DebugElement, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { EntryComponentMocker } from '../../dialogs/entry/entry.component.spec';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { ShiftChangeProtocolComponent } from '../../dialogs/shift-change-protocol/shift-change-protocol.component';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';
import { AuthenticationService } from '../../services/authentication.service';
import { FinishedNotificationsComponent } from '../../lists/finished-notifications/finished-notifications.component';
import { FutureNotificationsComponent } from '../../lists/future-notifications/future-notifications.component';
import { OpenNotificationsComponent } from '../../lists/open-notifications/open-notifications.component';
import { MockComponent } from '../../testing/mock.component';
import { OverviewComponent } from './overview.component';
import { Router } from '@angular/router';
import { User } from '../../model/user';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { LoginCredentials } from '../../model/login-credentials';
import { MdDialogModule, MaterialModule, MdDialog, MdDialogConfig, Overlay, OverlayContainer, OVERLAY_PROVIDERS } from '@angular/material';
import { SessionContext } from '../../common/session-context';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { AppModule } from '../../app.module';
import { NgModule } from '@angular/core';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { NotificationService } from '../../services/notification.service';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { ResponsibilityService } from '../../services/responsibility.service';
import { RESPONSIBILITIES } from '../../test-data/responsibilities';
import { ReminderService } from '../../services/reminder.service';
import { CurrentRemindersComponent } from '../../lists/current-reminders/current-reminders.component';
import { OPEN_NOTIFICATIONS } from '../../test-data/notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessageService } from '../../services/message.service';
import { FutureNotificationsMocker } from '../../lists/future-notifications/future-notifications.component.spec';
import { OpenNotificationsMocker } from '../../lists/open-notifications/open-notifications.component.spec';
import { FinishedNotificationsMocker } from '../../lists/finished-notifications/finished-notifications.component.spec';
import { AbstractListMocker } from '../../lists/abstract-list/abstract-list.component.spec';
import { FilterMocker } from '../../filter/filter.component.spec';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}
@NgModule({
  declarations: [EntryComponent, ShiftChangeProtocolComponent],
  entryComponents: [
    EntryComponent, ShiftChangeProtocolComponent
  ]
})
class TestModule { }

describe('OverviewComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;
  let router: Router;
  let mockNotificationService: MockNotificationService;
  let mockRespService: MockResponsibilityService;
  let messageService: MessageService;

  const correctUser: User = {
    id: '44', username: 'carlo', password: 'serverPwd'
    , name: 'Carlo Cottura', specialUser: true, itemName: this.userName
  };
  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getFinishedNotifications(notificationType: string) {
      this.loadCalled = true;
      return this;
    };
  }
  class MockResponsibilityService extends AbstractMockObservableService {
    plannedResponsibilities = null;
    responsibilities = null;

    getPlannedResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.plannedResponsibilities;
      return resptService;
    };
    getResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.responsibilities;
      return resptService;
    }
  }
  class MockAuthService extends AbstractMockObservableService {

    login(creds: LoginCredentials) {
      return this;
    }
  }
  class MockRespService extends AbstractMockObservableService {

    login(creds: LoginCredentials) {
      return this;
    }
  }
  let mockAuthService;
  beforeEach(async(() => {
    mockRespService = new MockResponsibilityService();
    mockNotificationService = new MockNotificationService();
    mockAuthService = new MockAuthService();
    router = new FakeRouter() as any as Router;
    messageService = new MessageService();

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [EntryComponent, ShiftChangeProtocolComponent]
      }
    });

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MdDialogModule,
        BrowserAnimationsModule,
        MaterialModule.forRoot()],
      declarations: [
        StringToDatePipe,
        FormattedTimestampPipe,
        AbstractListComponent,
        OverviewComponent,
        EntryComponent,
        MainNavigationComponent,
        ShiftChangeProtocolComponent,
        MockComponent({ selector: 'app-version-info' }),
        MockComponent({ selector: 'app-reminder-caller-job-component' }),
        EntryComponentMocker.getComponentMocks(),
        FilterMocker.getComponentMocks(),
        MockComponent({ selector: 'input', inputs: ['options'] }),
        AbstractListMocker.getComponentMocks(),
        OpenNotificationsMocker.getComponentMocks(true),
        FinishedNotificationsMocker.getComponentMocks(),
        FutureNotificationsMocker.getComponentMocks(),
        // TODO: the following are only needed because we do not mock away "ShiftChangeProtocolComponent"
        MockComponent({
          selector: 'app-responsibility', inputs: [
            'responsiblitySelection']
        }),
        MockComponent({
          selector: 'app-message-banner'
        }),
      ],
      providers: [
        { provide: NotificationService, useValue: mockNotificationService },
        { provide: Router, useValue: router },
        { provide: Overlay, useClass: Overlay },
        { provide: OVERLAY_PROVIDERS, useClass: OVERLAY_PROVIDERS },
        { provide: SessionContext, useValue: sessionContext },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: ResponsibilityService, useValue: mockRespService },
        { provide: ReminderService, useClass: ReminderService },
        { provide: MessageService, useValue: messageService }
      ],
    }).compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
  });

  it('should init correctly with plannedResponsibility if available', async(() => {
    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.responsibilitiesContainer.length).toBe(RESPONSIBILITIES.length);
    });
  }));

  it('should init correctly with plannedResponsibility and open a shiftChangeProtocoll dialog', async(() => {
    let shiftChangeProtocollOpened = false;
    let dialog;

    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      shiftChangeProtocollOpened = true;
    });

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      console.log(shiftChangeProtocollOpened);
      console.log(dialog);

      expect(shiftChangeProtocollOpened).toBe(true);
    });
  }));

  it('should init correctly with empty plannedResponsibility', async(() => {
    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = [];
    fixture.detectChanges();
    expect(component.responsibilitiesContainer).toBeUndefined();
  }));

  it('should init correctly with plannedResponsibility not available', async(() => {
    sessionContext.setCurrUser(correctUser);
    mockRespService.error = 'error';
    fixture.detectChanges();
    expect(component.responsibilitiesContainer).toBeUndefined();
  }));

  it('should open dialog to create new notification on button click', async(() => {
    let entryOpend = false;

    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;

    fixture.detectChanges();
    fixture.whenStable().then(() => { // wait for async getFinishedNotifications
      fixture.detectChanges(); // update view with array
      let des: DebugElement[];
      component.dialog.afterOpen.subscribe(() => entryOpend = true);
      des = fixture.debugElement.queryAll(By.css('.btn-primary'));
      click(des[0]);
      expect(entryOpend).toBe(true);
    });
  }));

  it('should open special dialog to create new notification on button click', async(() => {
    let entryOpend = false;
    let dialog;
    let de: DebugElement;

    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;
    fixture.detectChanges();

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      entryOpend = true;
    });
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      de = fixture.debugElement.query(By.css('.btn-info'));
      click(de);
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
    });
  }));

  it('should open a dialog to edit a notification on button click', async(() => {

    let entryOpend = false;
    let dialog;
    const testNotification = OPEN_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogEditEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(true);

    });
  }));

  it('should open a dialog to view a notification on button click', async(() => {

    let entryOpend = false;
    let dialog;
    const testNotification = OPEN_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogLookUpEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(false);

    });
  }));

});
