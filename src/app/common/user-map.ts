/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import {User} from '../model/user';

export class UserMap {
    private mappedUsers: User[];

    constructor( allUsers: User[] ) {
        this.mappedUsers = [];
        if ( !allUsers ) {
            console.log('UserMap was created without any Users!');
            throw new EvalError('UserMap was created without any Users!');
        }
        for ( const usr of allUsers ) {
            this.mappedUsers[ usr.username ] = usr;
        }
    }

    public findUser( usrShort: string ): User {
        return this.mappedUsers[ usrShort ];
    }

    public findAndRenderUser( shortUsr: string ): string {
        const usr = this.findUser(shortUsr);
        if ( !usr ) {
            return '[' + shortUsr + ']';
        } else {
         return usr.name;
        }
    }
}
