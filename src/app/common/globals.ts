/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
export class Globals {
    static FRONTEND_VERSION = '1.2.0';
    static SESSION_TOKEN_TAG = 'X-XSRF-TOKEN';
    static CURRENT_USER = 'CURRENT_USER';
    static ALL_USERS = 'ALL_USERS';
    static USER_SETTINGS = 'USER_SETTINGS';
    static BASE_URL = '/elogbook/rest/beservice';
    static BRANCHESNAME = 'BRANCHES';
    static STATUSES = 'STATUSES';
    static PRIORITIES = 'PRIORITIES';
    static FILTER_MATRIX = 'FILTER_MATRIX';
    static DATE_RANGE_FUTURE = 'DATE_RANGE_FUTURE';
    static DATE_RANGE_PAST = 'DATE_RANGE_PAST';
    static DATE_RANGE_HISTORY = 'DATE_RANGE_HISTORY';
    static GRID_TERRITORIES = 'TERRITORIES';
    static ACCESS_TOKEN = 'ACCESS_TOKEN';
    static BRANCHES = class Branches {
        static power = 'S';
        static gas = 'G';
        static heating = 'F';
        static water = 'W';
        static cfm = 'Z';
    };
    static NOTIFICATION_HISTORY_EXPANSION_STATES = 'NOTIFICATION_HISTORY_EXPANSION_STATES';
    static SEARCHOBJECT = 'SEARCHOBJECT';

    static COLLAPSE_STATE_ID_EXT = '_COLLAPSE_STATE_ID';
    static COLLAPSE_STATE_OPEN = 'COLLAPSE_STATE_OPEN';
    static COLLAPSE_STATE_PAST = 'COLLAPSE_STATE_PAST';
    static COLLAPSE_STATE_FUTURE = 'COLLAPSE_STATE_FUTURE';
    static LOCALSTORAGE_SESSION_ID = '/elogbook/session-id';

    static OAUTH2CONF_CLIENTID = 'elogbook-backend-fd';
    static OAUTH2CONF_SUPERUSER_ROLE = 'elogbook-superuser';

    // Switch to de/-activate ReminderJobPolling
    static REMINDER_JOB_POLLING_ON = true;

    static REMINDER_JOB_POLLING_INTERVALL = 60000;
    static REMINDER_JOB_POLLING_START_DELAY = 2000;

    // Switch to de/-activate ImportJobPolling
    static IMPORT_JOB_POLLING_ON = true;

    static IMPORT_JOB_POLLING_INTERVALL = 60000;
    static IMPORT_JOB_POLLING_START_DELAY = 2000;
    static SORTING_STATE = 'SORTING_STATE';
    static SORTING_STATE_GRID_ID_OPEN_NOTS = 'OverviewOpenNot';

    static NOTIFICATION_TYPE_PRESENCE_CHECK = 'presence-check';
    static PRESENCE_CHECK_TEXT = 'Meldung für Türkontakt/Anlagenbegehung';
    static NOTIFICATION_STATUS_OFFEN = 1;

}
