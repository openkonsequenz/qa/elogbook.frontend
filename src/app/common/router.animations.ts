/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { trigger, state, animate, style, transition } from '@angular/animations';

export  function  slideInOpacity() {
    return  trigger('slideInOpacity', [
        state('void',  style({  opacity:  0 })),
        state('*',  style({  opacity:  1  })),
        transition(':enter', [
            style({ transform:  'scale(1)' }),
            animate(500)
        ])
    ]);
} 
