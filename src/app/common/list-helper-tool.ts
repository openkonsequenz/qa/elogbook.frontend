/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
export class ListHelperTool {

public checkIfToday( compareDateString: string ): boolean {
    const now = new Date();
    const compareDate: Date = new Date( Date.parse(compareDateString) );

    if ( !compareDate ) {
      return false;
    }

    return  compareDate.getFullYear() === now.getFullYear() &&
            compareDate.getMonth() === now.getMonth() &&
            compareDate.getDate() === now.getDate();
  }
}