/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { TestBed, async, inject } from '@angular/core/testing';
import { ListHelperTool } from './list-helper-tool';

describe('ListHelperTool', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
      
      });
  
    });
  
    it('should detect today correctly', () => {
        const tool = new ListHelperTool();
        expect( tool.checkIfToday(null)).toBeFalsy();
        const today = new Date();
        today.setHours( 12, 13, 14, 59 );
        expect( tool.checkIfToday(today.toISOString()) ).toBeTruthy();

        today.setHours( 25, 1, 1, 1 );
        expect( tool.checkIfToday(today.toISOString()) ).toBeFalsy();
    });
});