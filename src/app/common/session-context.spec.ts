/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SessionContext } from './session-context';
import { User } from '../model/user';
import { USERS } from '../test-data/users';
import { UserMap } from 'app/common/user-map';

describe('SessionContext', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionContext]
    });

  });

  it('can instatiate service whe inject service', inject([SessionContext], (service: SessionContext) => {
    expect(service instanceof SessionContext);
  }));

  it('can store a SessionId and a User', inject([SessionContext], (service: SessionContext) => {
    service.setCurrSessionId('SpecialSessionId');
    const usr = new User();
    usr.id = '44';
    usr.name = 'Rudi';
    service.setCurrUser(usr);
    expect(service.getCurrSessionId()).toBe('SpecialSessionId');
    expect(service.getCurrUser()).not.toBeNull();
    expect(service.getCurrUser().id).toBe('44');
    expect(service.getCurrUser().name).toBe('Rudi');
  }));

  it('create a usermapper should fail when empty', inject([SessionContext], (service: SessionContext) => {
    let errorOccured = false;
    try {
      const um = service.getUserMap();
    } catch ( ea ) {
      errorOccured = true;
    }

    expect( errorOccured ).toBe( true );
  }));

  it('create a usermapper should work with all users set', inject([SessionContext], (service: SessionContext) => {
    let errorOccured = false;
    let um: UserMap;
    service.setAllUsers(USERS);
    try {
      um = service.getUserMap();
    } catch ( ea ) {
      errorOccured = true;
    }

    expect( errorOccured ).toBe( false );
    expect( um.findUser('otto').username ).toBe('otto');
  }));

});
