/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { TestBed, async, inject } from '@angular/core/testing';
import { User } from '../model/user';
import { USERS } from '../test-data/users';
import { UserMap } from 'app/common/user-map';


describe('UserMap', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
      });
  
    });
  
    it('can render a valid user', () => {
        const userMap = new UserMap( USERS );
        expect( userMap.findAndRenderUser('otto')).toBe('Otto Normalverbraucher');
    });

    it('can render a unknown user', () => {
        const userMap = new UserMap( USERS );
        expect( userMap.findAndRenderUser('Unknown')).toBe('[Unknown]');
    });

    it('throws an exception when not initialized', () => {
        expect( function() { new UserMap( null ); }).toThrowError( EvalError );
    });
});
  
