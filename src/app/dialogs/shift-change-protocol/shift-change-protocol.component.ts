/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, Optional, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { SessionContext } from '../../common/session-context';
import { User } from '../../model/user';
import { Notification } from '../../model/notification';
import { FilterMatrix } from '../../model/controller-model/filter-matrix';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { ResponsibilityService } from '../../services/responsibility.service';
import { BannerMessage } from '../../common/banner-message';
import { BannerMessageStatusEn } from '../../common/enums';
import { NotificationService } from '../../services/notification.service';
import { MessageService } from '../../services/message.service';
import { ErrorType } from '../../common/enums';
import { StatusEn } from '../../common/enums';

@Component({
  selector: 'app-shift-change-protocol',
  templateUrl: './shift-change-protocol.component.html',
  styleUrls: ['./shift-change-protocol.component.css', '../../lists/abstract-list/abstract-list.component.css'],
})
export class ShiftChangeProtocolComponent implements OnInit { 

  @Output() onLookUpNotification = new EventEmitter<Notification>();
  
  private dialogConfig = new MdDialogConfig();
  user: User = null;
  responsibilitiesContainer: TerritoryResponsibility[];
  notificationService: NotificationService;

  constructor(  public dialog: MdDialog,
    @Optional() public dialogRef: MdDialogRef<ShiftChangeProtocolComponent>,
    private responsibilityService: ResponsibilityService,
    public messageService: MessageService,
    private sessionContext: SessionContext) { }

    
  ngOnInit() {
    this.dialogConfig.disableClose = true;
    this.user = this.sessionContext.getCurrUser();

  }
  
  setResponsibilitiesContainer(responsibilitiesContainer: TerritoryResponsibility[]) {
    this.responsibilitiesContainer = responsibilitiesContainer;
  }
  
  close() {
    this.dialogRef.close();
    this.messageService.deactivateMessage();
  }

  openDialogLookUpEntry(notification: Notification) {
  
    const lookupDlgRef = this.dialog.open(EntryComponent, this.dialogConfig);
  // TODO: check init of sessionContext
    lookupDlgRef.componentInstance.user = this.user;
    lookupDlgRef.componentInstance.setNotification(notification);
    lookupDlgRef.componentInstance.isReadOnlyDialog = true;
    lookupDlgRef.afterClosed().subscribe(result => {
    });
  }
}
