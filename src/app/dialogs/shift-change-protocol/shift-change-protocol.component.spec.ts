/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter, SimpleChange } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdDialogModule, MaterialModule, MdDialog, MdDialogConfig, Overlay, OverlayContainer, OVERLAY_PROVIDERS } from '@angular/material';
import { MdDialogRef } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { ShiftChangeProtocolComponent } from './shift-change-protocol.component';
import { Responsibility } from '../../model/responsibility';
import { User } from '../../model/user';
import { RESPONSIBILITIES } from '../../test-data/responsibilities';
import { DUMMY_CREATED_NOTIFICATION, OPEN_NOTIFICATIONS } from '../../test-data/notifications';
import { click } from '../../testing';
import { NgModule } from '@angular/core';
import { MockComponent } from '../../testing/mock.component';
import { ResponsibilityService } from '../../services/responsibility.service';
import { NotificationService } from '../../services/notification.service';
import { MessageService } from '../../services/message.service';
import { SessionContext } from '../../common/session-context';
import { BannerMessage } from '../../common/banner-message';
import { BannerMessageStatusEn } from '../../common/enums';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { EntryComponentMocker } from '../../dialogs/entry/entry.component.spec';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { FutureNotificationsMocker } from '../../lists/future-notifications/future-notifications.component.spec';

@NgModule({
  declarations: [EntryComponent, ShiftChangeProtocolComponent],
  entryComponents: [
    EntryComponent, ShiftChangeProtocolComponent
  ]
})
class TestModule { }

describe('ShiftChangeProtocolComponent', () => {
  let component: ShiftChangeProtocolComponent;
  let fixture: ComponentFixture<ShiftChangeProtocolComponent>;
  let mockService;
  let mockNotificationService: MockNotificationService;

  const correctUser: User = {
    id: '44', username: 'carlo', password: 'serverPwd'
    , name: 'Carlo Cottura', specialUser: true, itemName: this.userName
  };

  class MockDialogRef extends AbstractMockObservableService {
    close() { }
  }

  class MockBtbService extends AbstractMockObservableService {
    getPlanedResponsibilities() {
      return this;
    };
    confirmResponsibilities(resp: Responsibility[]) {
      return this;
    };
  }

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getFinishedNotifications(notificationType: string) {
      this.loadCalled = true;
      return this;
    };

    public getNotificationVersions(incidentId: number) {
      const notifaction = DUMMY_CREATED_NOTIFICATION;
      return Observable.of(notifaction);
    }

  }

  let sessionContext;
  let messageService;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockService = new MockBtbService();
    mockNotificationService = new MockNotificationService();
    messageService = new MessageService();

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [EntryComponent]
      }
    });

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MdDialogModule,
        BrowserAnimationsModule,
        MaterialModule.forRoot()],
      declarations: [
        StringToDatePipe,
        FormattedTimestampPipe,
        ShiftChangeProtocolComponent,
        EntryComponent,
        MockComponent({ selector: 'input', inputs: ['options'] }),
        EntryComponentMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-message-banner'
        }),
        MockComponent({
          selector: 'app-responsibility',
          inputs: [
            'responsiblitySelection',
            'withCheckboxes',
            'withEditButtons',
            'isCollapsible']
        }),
        FutureNotificationsMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-open-notifications',
          inputs: [
            'responsiblitySelection',
            'withCheckboxes',
            'withEditButtons',
            'isCollapsible',
            'stayHidden',
            'enforceShowReadOnly',
            'gridId'
          ]
        }),
        MockComponent({
          selector: 'app-finished-notifications ',
          inputs: [
            'responsiblitySelection',
            'withCheckboxes',
            'withEditButtons',
            'isCollapsible',
            'stayHidden',
            'enforceShowReadOnly',
            'gridId'
          ]
        }),
      ],
      providers: [
        { provide: MdDialogRef, useClass: MockDialogRef },
        { provide: Overlay, useClass: Overlay },
        { provide: OVERLAY_PROVIDERS, useClass: OVERLAY_PROVIDERS },
        { provide: MessageService, useValue: messageService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: NotificationService, useValue: mockNotificationService },
        { provide: ResponsibilityService, useValue: mockService },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftChangeProtocolComponent);
    component = fixture.componentInstance;
  });

  it('should open dialog to lookup a notification on button click', async(() => {
    spyOn(component.dialogRef, 'close').and.callThrough();
    spyOn(component.messageService, 'deactivateMessage').and.callThrough();
    let entryOpend = false;
    let dialog;
    
    sessionContext.setCurrUser(correctUser);
    mockNotificationService.content = OPEN_NOTIFICATIONS;
    const testNotification = OPEN_NOTIFICATIONS[0];
    mockService.plannedResponsibilities = RESPONSIBILITIES;
    fixture.detectChanges();
    
    component.dialog.afterOpen.subscribe((value) => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
      
    });
  
    component.openDialogLookUpEntry(testNotification);
    
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(false);
      component.close();
      expect(component.dialogRef.close).toHaveBeenCalled();
      expect(component.messageService.deactivateMessage).toHaveBeenCalled();
    });
  }));

  
});
