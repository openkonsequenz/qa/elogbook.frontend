/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, Optional } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { MessageService, MessageDefines } from '../../services/message.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent {

  constructor(
    @Optional() public dialogRef: MdDialogRef<LogoutComponent>,
    private msgService: MessageService,
    private authService: AuthenticationService,
    private router: Router) {}

  logout() {
    this.authService.logout().subscribe(res => {      
      this.dialogRef.config.data.logout();
      this.msgService.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);
      this.dialogRef.close();
      this.router.navigate(['/logout']);      
    },
      error => { 
        console.log(error);
      }
    );    
  }
}
