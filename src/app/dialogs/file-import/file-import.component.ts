/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, Input, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { NotificationFileModel } from '../../model/file-model';
import { ImportService } from '../../services/import.service';
import { SessionContext } from '../../common/session-context';
import { BannerMessage } from '../../common/banner-message';
import { StatusEn, BannerMessageStatusEn } from '../../common/enums';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';

@Component({
  selector: 'app-file-import',
  templateUrl: './file-import.component.html',
  styleUrls: ['./file-import.component.css', '../../lists/abstract-list/abstract-list.component.css']
})
export class FileImportComponent implements OnInit {
  gridId = 'ImportNotificationDialog';
  defaultList: NotificationFileModel[] = new Array<NotificationFileModel>();
  defaultList1: NotificationFileModel[];
  defaultList2: NotificationFileModel[];
  fileModels: NotificationFileModel[];
  importFileModel: NotificationFileModel;
  isImportFileSelected = false;
  subscriptionImportFiles: Subscription;
  subscriptionDeleteFiles: Subscription;

  constructor(public dialogRef: MdDialogRef<FileImportComponent>,
    private importService: ImportService,
    public sessionContext: SessionContext) { }

  ngOnInit() {
    this.importFileModel = null;
    this.isImportFileSelected = false;
    this.setFiles();
  }

  setFiles() {
    this.isImportFileSelected = false;
    this.importService.getImportFiles().subscribe(files => {
      this.fileModels = files;
      this.defaultList1 = Object.assign(new Array<NotificationFileModel>(), files);
      this.setFilesForBrachAndTer();
      // if only one file is available, use direct import
      if (this.fileModels.length === 1) {
        this.importThisFile(this.defaultList[0]);
      }
    },
      error => {
        this.sessionContext.setBannerMessage(
          BannerMessageStatusEn.error,
          'Fehler beim Lesen der Dateien von der Datenbank. /1/',
          true);
        this.dialogRef.close();
      });
  }

  setFilesForBrachAndTer() {
    this.importService.importFile().subscribe(files => {
      this.fileModels = files;
      this.defaultList2 = Object.assign(new Array<NotificationFileModel>(), files);
      this.mergeArrays();
      // if only one file is available, use direct import
      if (this.fileModels.length === 1) {
        this.importThisFile(this.defaultList[0]);
      }
    },
      error => {
        this.sessionContext.setBannerMessage(
          BannerMessageStatusEn.error,
          'Fehler beim Lesen der Dateien von der Datenbank. /2/',
          true);
        this.dialogRef.close();
      });


  }

  mergeArrays() {
    for (let index = 0; index < this.defaultList1.length; index++) {

      const item = new NotificationFileModel();
      const objDateFileName = this.defaultList1[index];
      const objBranchTerAndDescr = this.defaultList2[index];

      item.creationDate = objDateFileName.creationDate;
      item.fileName = objDateFileName.fileName;
      item.branchName = objBranchTerAndDescr.branchName;
      item.gridTerritoryName = objBranchTerAndDescr.gridTerritoryName;
      item.notificationText = objBranchTerAndDescr.notificationText;

      this.defaultList.push(item);
    }

  }


  importThisFile(fileModel: NotificationFileModel) {
    this.importFileModel = fileModel;
    this.isImportFileSelected = true;
    this.deleteFile(fileModel.fileName);
  }

  deleteFile(fileName: string) {
    this.importService.deleteImportFile(fileName).subscribe(result => {
      this.dialogRef.close();
    },
      error => {
        this.sessionContext.setBannerMessage(
          BannerMessageStatusEn.error,
          'Fehler beim Löschen der Datei.',
          true);
        this.dialogRef.close();
      }
    );
  }

  cancel() {
    this.dialogRef.close();
  }
}
