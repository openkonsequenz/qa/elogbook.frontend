/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { FormsModule } from '@angular/forms';
import { async, fakeAsync, ComponentFixture, TestBed, tick, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Injectable, DebugElement } from '@angular/core';
import { click, newEvent } from '../../testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { Observable } from 'rxjs/Observable';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { MockComponent } from '../../testing/mock.component';
import { MdDialogRef } from '@angular/material';
import { Globals } from '../../common/globals';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { SessionContext } from '../../common/session-context';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';
import { FileImportComponent } from './file-import.component';
import { ImportService } from '../../services/import.service';

import { IMPORT_NOTIFICATION_FILES } from '../../test-data/import-notification-files';

describe('FileImportComponent', () => {
  let component: FileImportComponent;
  let fixture: ComponentFixture<FileImportComponent>;
  let sessionContext: SessionContext;
  let mockImportService;

  class MockDialogRef extends AbstractMockObservableService {
    close() { }
  }

  class MockImportService extends AbstractMockObservableService {

    getImportFiles() {
      this.content = IMPORT_NOTIFICATION_FILES;

      return this;
    };
    importFile() {
      this.content = IMPORT_NOTIFICATION_FILES;

      return this;
    };
  }


  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockImportService = new MockImportService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        FileImportComponent,
        StringToDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'input', inputs: ['options'] })
      ],
      providers: [
        { provide: MdDialogRef, useClass: MockDialogRef },
        { provide: ImportService, useValue: mockImportService },
        { provide: SessionContext, useClass: SessionContext }
      ]})
      .overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [FileImportComponent]
        }
      
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should show files initially', async(() => {

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.fileModels.length).toBe(2);
      expect(component.isImportFileSelected).toBe(false);
    });
  }));

  xit('should call import file after import button click', async(() => {
    spyOn(component, 'importThisFile').and.callThrough();
    spyOn(component, 'deleteFile').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const des = fixture.debugElement.queryAll(By.css('td > .btn-sm'));
      click(des[0]);
      expect(component.importThisFile).toHaveBeenCalledWith(IMPORT_NOTIFICATION_FILES[0]);
      expect(component.isImportFileSelected).toBe(true);
      expect(component.deleteFile).toHaveBeenCalledWith(IMPORT_NOTIFICATION_FILES[0].fileName);     
    });
  }));


});
