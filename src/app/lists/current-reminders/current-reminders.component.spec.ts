/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter } from '@angular/core';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { Notification } from '../../model/notification';
import { click } from '../../testing/index';
import { FormsModule } from '@angular/forms';
import { REMINDER_NOTIFICATIONS } from '../../test-data/reminder-notifications';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { SessionContext } from '../../common/session-context';
import { CurrentRemindersComponent } from './current-reminders.component';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { ResponsibilityService } from '../../services/responsibility.service';
import { ReminderSearchFilter } from '../../model/reminder-search-filter';
import { SortingComponent } from '../../lists/sorting/sorting.component';
import { FilterMatrix } from '../../model/controller-model/filter-matrix';
import { MessageService } from '../../services/message.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { MockComponent } from '../../testing/mock.component';
import { FILTER_MATRIX_NONE_SELECTED } from '../../test-data/filter-matrix';
import { FILTER_MATRIX_ALL_SELECTED } from '../../test-data/filter-matrix';
import { LoadingSpinnerComponent } from '../../dialogs/loading-spinner/loading-spinner.component';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { UserService } from 'app/services/user.service';

describe('CurrentRemindersComponent', () => {
  let component: CurrentRemindersComponent;
  let fixture: ComponentFixture<CurrentRemindersComponent>;

  class MockReminderService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    reminderSearchFilter: ReminderSearchFilter = { reminderDate: '', responsibilityFilterList: [] };

    public getCurrentReminders(reminderSearchFilter: ReminderSearchFilter) {
      this.loadCalled = true;
      this.reminderSearchFilter = reminderSearchFilter;
      return this;
    }
  }

  class MockMessageService extends AbstractMockObservableService {
    public loginLogoff$: EventEmitter< string > = new EventEmitter< string >();    
    public matrixFilterChanged$: EventEmitter<Number[]> = new EventEmitter< Number[] >();
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    };

    getUserSettings() {
      return this;
    };

    postUserSettings() {
      return this;
    };    
  } 

  let mockMessageService;
  let mockReminderService;
  let sessionContext;
  let mockUserService;

  beforeEach(async(() => {
    mockReminderService = new MockReminderService();
    mockMessageService = new MockMessageService();
    sessionContext = new SessionContext();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        CurrentRemindersComponent, StringToDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'app-loading-spinner' }),
        SortingComponentMocker.getComponentMocks(),
      ],
      providers: [
        { provide: ReminderService, useValue: mockReminderService },
        { provide: MessageService, useClass: MessageService },
        { provide: NotificationService, useValue: mockReminderService },        
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: ResponsibilityService }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentRemindersComponent);
    component = fixture.componentInstance;
  });

  it('should retrieve all current reminders', async(() => {
    const resps = REMINDER_NOTIFICATIONS;

    fixture.detectChanges();

    mockReminderService.subscribe(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        const des = fixture.debugElement.queryAll(By.css('td > .btn-sm'));
        expect(des.length).toBe(resps.length);
        expect(mockReminderService.loadCalled).toBe(true);
      });
    });
    mockReminderService.content = resps;
  }));

  it('should raise edit emitter event when EDIT clicked', async(() => {
    const resps = REMINDER_NOTIFICATIONS;
   
    component.ngOnInit();
    mockReminderService.subscribe(() => {
      fixture.detectChanges();
      mockReminderService.itemChanged$.emit(mockReminderService.content);

      fixture.whenStable().then(() => { // wait for async getCurrentReminders
        fixture.detectChanges(); // update view with array
        component.onEditNotification.subscribe((notification: Notification) => expect(notification).toBe(resps[0]));
        const des = fixture.debugElement.queryAll(By.css('td > .btn-sm'));
        click(des[0]);
      });
    });
    mockReminderService.content = resps;
  }));

  it('should call getCurrentReminders after notification modified', async(() => {
    fixture.detectChanges();
    mockReminderService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockReminderService.itemChanged$.emit(REMINDER_NOTIFICATIONS);
    fixture.whenStable().then(() => { // wait for async getCurrentReminders
      fixture.detectChanges(); // update view with array

      expect(mockReminderService.loadCalled).toBe(true);
    });
  }));

 it('should run correctly in setError ', async(() => {
    mockReminderService.error = 'Connection Error';
    fixture.detectChanges();

    fixture.whenStable().then(() => { // wait for async getCurrentReminders
      fixture.detectChanges();        // update view with array
    });
  }));

  it('should set the reminderSearchFilter.responsibilityFilterList to empty list after no selection in filtermatrix', async(() => {
    
    sessionContext.setfilterMatrix(FILTER_MATRIX_NONE_SELECTED);
    fixture.detectChanges();
    
    fixture.whenStable().then(() => {
      expect(mockReminderService.reminderSearchFilter.responsibilityFilterList.length).toBe(0);      
    });   
  }));

  it('should set the reminderSearchFilter.responsibilityFilterList after selecting all in filtermatrix', async(() => {    
    sessionContext.setfilterMatrix(FILTER_MATRIX_ALL_SELECTED);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(mockReminderService.reminderSearchFilter.responsibilityFilterList.length).toBe(12);      
    });   
  }));

  it('should init reminderSearchFilter.responsibilityFilterList as undefined w filtermatrix in sessionContext is null', async(() => {    
    sessionContext.setfilterMatrix(null);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(mockReminderService.reminderSearchFilter.responsibilityFilterList).toBe(undefined);      
    });   
  }));


});

