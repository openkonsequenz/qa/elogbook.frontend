/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter, SimpleChange } from '@angular/core';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { Notification } from '../../model/notification';
import { click } from '../../testing/index';
import { FormsModule } from '@angular/forms';
import { OPEN_NOTIFICATIONS } from '../../test-data/notifications';
import { SEARCH_RESULT_NOTIFICATIONS2 } from '../../test-data/search-result-notifications';
import { DUMMY_NOTIFICATION } from '../../test-data/notifications';
import { DUMMY_CREATED_NOTIFICATION } from '../../test-data/notifications';
import { DUMMY_UPDATED_NOTIFICATION } from '../../test-data/notifications';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { SearchResultService } from '../../services/search-result.service';
import { SessionContext } from '../../common/session-context';
import { SearchResultListComponent } from './search-result-list.component';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { ResponsibilityService } from '../../services/responsibility.service';
import { MockComponent } from '../../testing/mock.component';
import { LoadingSpinnerComponent } from '../../dialogs/loading-spinner/loading-spinner.component';
import { MessageService } from '../../services/message.service';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { BRANCHES } from '../../test-data/branches';
import { GRIDTERRITORIES } from '../../test-data/gridterritories';
import { STATUSES } from '../../test-data/statuses';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { UserService } from 'app/services/user.service';
import { USERSETTING_1 } from 'app/test-data/user-settings';

describe('SearchResultListComponent', () => {
  let component: SearchResultListComponent;
  let fixture: ComponentFixture<SearchResultListComponent>;
  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getOpenNotifications(notificationType: string) {
      this.loadCalled = true;
      return this;
    };
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    };

    getUserSettings() {
      return this;
    };

    postUserSettings() {
      return this;
    };    
  }
  
  class MockSearchResultService extends AbstractMockObservableService {    
    
    public getSearchResults() {    
      return this;
    };
  }
  let messageService;
  let sessionContext: SessionContext;
  let mockService;
  let mockSearchResultService;
  let mockUserService;

  beforeEach(async(() => {
    messageService = new MessageService();
    sessionContext = new SessionContext();
    mockService = new MockNotificationService();
    mockSearchResultService = new MockSearchResultService();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        SearchResultListComponent,
        StringToDatePipe,
        FormattedTimestampPipe,
        SortingComponentMocker.getComponentMocks(),
        MockComponent({ selector: 'app-loading-spinner' })
      ],
      providers: [
        { provide: ReminderService, useValue: mockService },
        { provide: MessageService, useValue: messageService },
        { provide: SearchResultService, useValue: mockSearchResultService },
        { provide: NotificationService, useValue: mockService },
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: ResponsibilityService }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultListComponent);
    component = fixture.componentInstance;
    //component.territoryBranchFilterEmitter = new EventEmitter();
    //MessageService.matrixFilterChanged
  });

  it('should raise edit emitter event when EDIT clicked', async(() => {
    const notifications = OPEN_NOTIFICATIONS;
    mockService.content = notifications;
    component.ngOnInit();
    mockService.subscribe(() => {
      fixture.detectChanges();
      //component.territoryBranchFilterEmitter.emit(mockService.content);
      //MessageService.matrixFilterChanged

      fixture.whenStable().then(() => { // wait for async method 
        fixture.detectChanges(); // update view with array
        component.onEditNotification.subscribe((notification: Notification) => expect(notification).toBe(notifications[0]));
        //TODO const des = fixture.debugElement.queryAll(By.css('.btn-primary'));
        //TODO click(des[0]);
      });
    });
  }));

  it('should retrieve all notifications with given search parameter', async(() => {
    const notifications = OPEN_NOTIFICATIONS; // TODO: change to searchresult
    mockService.content = notifications;

    component.ngOnInit();
    mockService.subscribe(() => {
      fixture.detectChanges();
      //component.territoryBranchFilterEmitter.emit(mockService.content);
      //MessageService.matrixFilterChanged

      fixture.whenStable().then(() => {
        const des = fixture.debugElement.queryAll(By.css('.btn-primary')); // TODO: check condition
        //TODO expect(des.length).toBe(notifications.length);
      });
    });
    fixture.detectChanges();
  }));

  it('should call getSearchResultNotifications after new notification added', async(() => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load should be triggered by emit and not by init
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);
    fixture.whenStable().then(() => { // wait for async getSearchResultNotifications // TODO: change method
      fixture.detectChanges(); // update view with array
      //TODO expect(mockService.loadCalled).toBe(true);
    });
  }));

  it('should call getSearchResultNotifications after notification modified', async(() => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemChanged$.emit(DUMMY_UPDATED_NOTIFICATION);
    fixture.whenStable().then(() => { // wait for async getSearchResultNotifications  // TODO: change method
      fixture.detectChanges(); // update view with array

      //TODO expect(mockService.loadCalled).toBe(true);
    });
  }));

  it('should run correctly in setError ', async(() => {
    mockService.error = 'Connection Error';
    fixture.detectChanges();

    fixture.whenStable().then(() => { // wait for async getResponsibilities
      fixture.detectChanges();        // update view with array
    });
  }));

  it('should set class power on notifications with same branch', async(() => {
    
    sessionContext.setBranches(BRANCHES);
    sessionContext.setGridTerritories(GRIDTERRITORIES);
    sessionContext.setStatuses(STATUSES);
    sessionContext.setUsersSettings(USERSETTING_1);
    fixture.detectChanges(); 
    fixture.whenStable().then(() => {
    
    const notifications = SEARCH_RESULT_NOTIFICATIONS2; 
    mockSearchResultService.content = notifications;
    
    const currentSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
    currentSearchFilter.searchString = 'Test';
    currentSearchFilter.responsibilityForwarding = 'Max';
    currentSearchFilter.statusOpenSelection = false;
    currentSearchFilter.statusInWorkSelection = false;
    currentSearchFilter.statusDoneSelection = false;
    currentSearchFilter.statusClosedSelection = false;
    currentSearchFilter.fkRefBranch = 1;
    currentSearchFilter.fkRefGridTerritory = 2;
    currentSearchFilter.fastSearchSelected = false;
    component.globalSearchFilter = currentSearchFilter;
    let dePower, deGas, deHeating, deWater;
    
      component.isCollapsible = true;      
      component.stayHidden = false;
      
      component.showSpinner = false;
      mockSearchResultService.subscribe(() => { 
        fixture.detectChanges();
        messageService.matrixFilterChanged$.emit(mockSearchResultService.content);
        fixture.whenStable().then(() => {
          fixture.detectChanges(); 
          component.getSearchResults();
      
      component.showSpinner = false;   
      fixture.componentRef.changeDetectorRef.detectChanges();
            
      fixture.detectChanges();
      
      dePower = fixture.debugElement.nativeElement.querySelector('td.notification-tab-branch.power > span');
      deGas = fixture.debugElement.nativeElement.querySelector('td.notification-tab-branch.gas > span');
      deHeating = fixture.debugElement.nativeElement.querySelector('td.notification-tab-branch.heating > span');
      deWater = fixture.debugElement.nativeElement.querySelector('td.notification-tab-branch.water > span');
      expect(dePower.innerText.toString()).toContain('S');
      expect(deGas.innerText.toString()).toContain('G');
      expect(deHeating.innerText.toString()).toContain('F');
      expect(deWater.innerText.toString()).toContain('W');
    });
    });

  });

  }));

});
