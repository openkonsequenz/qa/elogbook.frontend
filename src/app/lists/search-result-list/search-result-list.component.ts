/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { Notification } from '../../model/notification';
import { StatusEn, ErrorType } from '../../common/enums';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { MessageService, MessageDefines } from '../../services/message.service';
import { SearchResultService } from '../../services/search-result.service';
import { NotificationService } from '../../services/notification.service'; //TODO remove this from abstract list
import { ReminderService } from '../../services/reminder.service'; //TODO remove this from abstract list
import { DaterangepickerConfig } from 'ng2-daterangepicker'; //TODO remove this from abstract list
import { SessionContext } from '../../common/session-context';
import { LoadingSpinnerComponent } from '../../dialogs/loading-spinner/loading-spinner.component';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-search-result-list',
  templateUrl: './search-result-list.component.html',
  styleUrls: ['./search-result-list.component.css', '../abstract-list/abstract-list.component.css'],
})
export class SearchResultListComponent extends AbstractListComponent implements OnChanges {
  column: string;

  @Input()
  globalSearchFilter: GlobalSearchFilter;
  oldNotification: Notification = null;
  incidentIdChange = true;


  spinCtrl = false;
  constructor(protected messageService: MessageService,
    protected searchResultService: SearchResultService,
    protected notificationService: NotificationService, protected reminderService: ReminderService,
    protected sessionContext: SessionContext,
    protected daterangepickerConfig: DaterangepickerConfig,
    protected userService: UserService) {
    super(messageService, notificationService, reminderService, sessionContext, daterangepickerConfig, userService);
  }

  public isStatusClassFinished(fkRefStatus: number): boolean {
    switch (fkRefStatus) {
      case StatusEn.done:
        return true;
      default:
        return false;
    }
  }

  getSearchResults(): void {
    this.searchResultService.getSearchResults(this.globalSearchFilter)
      .subscribe(resultNotifications => {
        this.setNotifications(resultNotifications);
        this.showSpinner = false;
      },
        error => {
          this.showSpinner = false;
          console.log(error);
          this.messageService.emitError('Suchergebnisliste', ErrorType.retrieve);
        }
      );
    this.showSpinner = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['globalSearchFilter'] !== undefined
      && changes['globalSearchFilter'].currentValue !== undefined) {
      this.oldNotification = null;
      this.incidentIdChange = false;
      this.getSearchResults();
    }
  }

  onItemChanged(notification: Notification): void {
    this.getSearchResults();
  }

  isBranchGas(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'gas';
  }

  isBranchPower(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'power';
  }

  isBranchWater(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'water';
  }

  isBranchHeating(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'heating';
  }

  isBranchCentralFaultMonitoring(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'cfm';
  }
}

