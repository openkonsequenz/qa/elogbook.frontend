/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component } from '@angular/core';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';
import { Notification } from '../../model/notification';
import { StatusEn, ErrorType } from '../../common/enums';
import { NotificationSearchFilter } from '../../model/notification-search-filter';
import * as moment from 'moment';
import { SortingComponent } from '../../lists/sorting/sorting.component';
import { DateRange } from '../../model/date-range';
import { Globals } from '../../common/globals';

@Component({
  selector: 'app-finished-notifications',
  templateUrl: './finished-notifications.component.html',
  styleUrls: ['./finished-notifications.component.css', '../abstract-list/abstract-list.component.css'],
})
export class FinishedNotificationsComponent extends AbstractListComponent {
  column: string;

  private getFinishedNotifications(): void {
    this.notificationService.getFinishedNotifications(this.notificationSearchFilter).subscribe(nots => {
       this.setNotifications(nots);
       this.showSpinner = false;
    },
      error => {
        console.log(error);
        this.messageService.emitError('Geschlossene Meldungen', ErrorType.retrieve);
      });
      this.showSpinner = true;
  }

  getNotifications(): void {
    this.notificationSearchFilter.dateFrom = moment(this.startDate).toISOString();
    this.notificationSearchFilter.dateTo = moment(this.endDate).toISOString();
    this.getFinishedNotifications();
  }

  getHistoricalNotifications(): void {
    this.getFinishedNotifications();
  }

  setDefaultDateRange(): void {
    const dateRange: DateRange = this.sessionContext.getDateRange(Globals.DATE_RANGE_PAST);
    if (dateRange) {
      this.startDate = new Date(dateRange.dateFrom);
      this.endDate = new Date(dateRange.dateTo);
    } else {
      this.startDate = new Date();
      this.startDate.setHours(this.startDate.getHours() - 168);
      this.startDate.setHours(0);
      this.startDate.setMinutes(0);
      this.startDate.setSeconds(0);
      this.endDate = new Date();
      this.endDate.setHours(23);
      this.endDate.setMinutes(59);
      this.endDate.setSeconds(59);
    }
  }

  onItemAdded(notification: Notification): void {
    this.getNotifications();
  }

  onItemChanged(notification: Notification): void {
    this.getNotifications();
  }

  isStatusClosed(notification: Notification): boolean {
    return (notification.fkRefNotificationStatus === StatusEn.closed);

  }

  storeDateRange(event) {
    const dateRange: DateRange = new DateRange();
    dateRange.dateFrom = event.picker.startDate;
    dateRange.dateTo = event.picker.endDate;
    this.sessionContext.setDateRange(dateRange, Globals.DATE_RANGE_PAST);
  }
}
