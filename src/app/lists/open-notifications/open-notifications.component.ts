/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component } from '@angular/core';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { Notification } from '../../model/notification';
import { StatusEn, ErrorType } from '../../common/enums';
import { SortingComponent } from '../../lists/sorting/sorting.component';



@Component({
  selector: 'app-open-notifications',
  templateUrl: './open-notifications.component.html',
  styleUrls: ['./open-notifications.component.css', '../abstract-list/abstract-list.component.css'],
})
export class OpenNotificationsComponent extends AbstractListComponent {
  column: string;

  protected init() {
    super.init();
  }
  getNotifications(): void {
    this.notificationService.getOpenNotifications(this.notificationSearchFilter).subscribe(nots => {
      this.setNotifications(nots);
      this.showSpinner = false;
    },
      error => {
        console.log(error);
        this.messageService.emitError('Offene Meldungen', ErrorType.retrieve); 
      }
    );
    this.showSpinner = true;
  }

  getHistoricalNotifications(): void {
   this.getNotifications();
  }

  onItemAdded(notification: Notification): void {
    this.getNotifications();
  }

  onItemChanged(notification: Notification): void {
    this.getNotifications();
  }

  public getStatusClassById(fkRefStatus: number): string {
    const className = '';

    switch (fkRefStatus) {
      case StatusEn.done:
        return 'finished';
      default:
        break;
    }
    return '';
  }

  public getReminderStatusClass(reminderDate: string, fkRefStatus: number): string {
    const className = '';
    let isReminder = false;
    const isOpenOrInWork =  (fkRefStatus === StatusEn.open) || (fkRefStatus === StatusEn.inWork);

    if (isOpenOrInWork && reminderDate != null && reminderDate.length > 0) {
        isReminder = (Date.parse(reminderDate) <= Date.now());
    } else {
      isReminder = false;
    }

    if (isReminder) {
        return 'current-reminder';
    }
    return '';
  }
}
