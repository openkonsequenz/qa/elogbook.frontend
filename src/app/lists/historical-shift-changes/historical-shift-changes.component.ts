/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HistoricalResponsibility } from '../../model/historical-responsibility';
import { ResponsibilityService } from '../../services/responsibility.service';
import { ResponsibilitySearchFilter } from '../../model/responsibility-search-filter';
import { DateRange } from '../../model/date-range';
import { Globals } from '../../common/globals';
import { SessionContext } from '../../common/session-context';

import * as moment from 'moment';

@Component({
  selector: 'app-historical-shift-changes',
  templateUrl: './historical-shift-changes.component.html',
  styleUrls: ['./historical-shift-changes.component.css']
})

export class HistoricalShiftChangesComponent implements OnInit {
  @Output() onShiftChangeSelected = new EventEmitter<number>();

  historicalResponsibilities: HistoricalResponsibility[];
  responsibilitySearchFilter = new ResponsibilitySearchFilter();
  selectedRow: number;
  endDate: Date;
  startDate: Date;

  constructor(
    protected responsibilityService: ResponsibilityService,
    protected sessionContext: SessionContext) { }

  ngOnInit() {
    this.setDefaultDateRange();
    this.retrieveHistoricalResponsibilities();
  }

  retrieveHistoricalResponsibilities(): void {
    this.responsibilitySearchFilter.transferDateFrom = moment(this.startDate).toISOString();
    this.responsibilitySearchFilter.transferDateTo = moment(this.endDate).toISOString();

    this.responsibilityService.getHistoricalShiftChangeList(this.responsibilitySearchFilter).subscribe(resp => {
      this.historicalResponsibilities = resp.historicalResponsibilities;
    });
  }

  setDefaultDateRange(): void {
    const dateRange: DateRange = this.sessionContext.getDateRange(Globals.DATE_RANGE_HISTORY);
    if (dateRange) {
      this.startDate = new Date(dateRange.dateFrom);
      this.endDate = new Date(dateRange.dateTo);
    } else {
      this.endDate = new Date();
      this.startDate = new Date();
      this.startDate.setDate(this.endDate.getDate() - 7);

      this.startDate.setHours(0);
      this.startDate.setMinutes(0);
      this.startDate.setSeconds(0);

      this.endDate.setHours(23);
      this.endDate.setMinutes(59);
      this.endDate.setSeconds(59);
    }
  }

  selectShiftChange(index: number, historicalResponsibility: HistoricalResponsibility) {
    this.selectedRow = index;
    this.onShiftChangeSelected.emit(historicalResponsibility.transactionId);
  }

  storeDateRange(event) {
    const dateRange: DateRange = new DateRange();
    dateRange.dateFrom = event.picker.startDate;
    dateRange.dateTo = event.picker.endDate;
    this.sessionContext.setDateRange(dateRange, Globals.DATE_RANGE_HISTORY);
  }

  mapUserName( shortUsr: string ) {
    const userMap = this.sessionContext.getUserMap();
    return userMap ? this.sessionContext.getUserMap().findAndRenderUser(shortUsr) : shortUsr;
  }    

}
