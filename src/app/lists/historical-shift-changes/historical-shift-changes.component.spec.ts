/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { HistoricalShiftChangesComponent } from './historical-shift-changes.component';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DaterangepickerConfig, Daterangepicker } from 'ng2-daterangepicker';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { MockComponent } from '../../testing/mock.component';
import { HISTORICAL_SCHIFTCHANGES} from '../../test-data/historical-shiftchanges';
import { By } from '@angular/platform-browser';
import { click } from '../../testing/index';
import { SessionContext } from '../../common/session-context';
import { Globals } from '../../common/globals';
import { DateRange } from '../../model/date-range';

describe('HistoricalShiftChangesComponent', () => {
  let component: HistoricalShiftChangesComponent;
  let fixture: ComponentFixture<HistoricalShiftChangesComponent>;
  let mockRespService: MockResponsibilityService;
  let router: Router;

  class MockEvent {
    picker: Picker = new Picker();
  }

  class Picker {
    startDate: string;
    endDate: string;
  }

  class FakeRouter {
    navigate(commands: any[]) {
      return commands[0];
    }
  }

  class MockResponsibilityService extends AbstractMockObservableService {    
    loadCalled = false;

    getHistoricalShiftChangeList() {   
      this.loadCalled = true;   
      return this;
    };
  }

  let sessionContext: SessionContext;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockRespService = new MockResponsibilityService();
    router = new FakeRouter() as any as Router;
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [HistoricalShiftChangesComponent,
        StringToDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'input', inputs: ['options'] }),
      ],
      providers: [
        { provide: ResponsibilityService, useValue: mockRespService },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: SessionContext, useClass: SessionContext },
        DaterangepickerConfig,
        Daterangepicker
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalShiftChangesComponent);
    component = fixture.componentInstance;    
  });

  it('should call setDefaultDateRange and set the startdate 7 days in the past', () => {        
    fixture.detectChanges();
    const tmpDate = new Date();
    tmpDate.setDate(component.endDate.getDate() - 7 );
    tmpDate.setHours(0);
    tmpDate.setMinutes(0);
    tmpDate.setSeconds(0);

    fixture.detectChanges();
    expect(component.startDate.getTime()).toBeCloseTo(tmpDate.getTime(), -5);          
  });

  it('should call retrieveHistoricalResponsibilities and set historicalResponsibilities', async(() => {           
    spyOn(component, 'retrieveHistoricalResponsibilities').and.callThrough();    
    const resps = HISTORICAL_SCHIFTCHANGES;
    mockRespService.content = resps;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();                 
      console.log(mockRespService.loadCalled);
      expect(component.retrieveHistoricalResponsibilities).toHaveBeenCalled();
      expect(component.historicalResponsibilities.length).toBe(resps.historicalResponsibilities.length);      
    });
  }));

  it('should call selectShiftChange on click', async(() => {       
    const resps = HISTORICAL_SCHIFTCHANGES;
    mockRespService.content = resps;
    fixture.detectChanges();            

    fixture.whenStable().then(() => { // wait for async getCurrentReminders
      fixture.detectChanges(); // update view with array
      const des = fixture.debugElement.queryAll(By.css('tr'));
      component.onShiftChangeSelected.subscribe((transactionId: number) => {
        expect(transactionId).toBe(resps.historicalResponsibilities[0].transactionId);
        expect(component.selectedRow).toBe(0);
      });
      //3rd 'tr' is one of the rows where the click listener is subscribed on     
      click(des[2]);      
    });          
  }));

  it('should store the picked date range in sessioncontext', async(() => {
    const mockEvent: MockEvent = new MockEvent();
    mockEvent.picker.startDate = '2017-09-03T22:00:00.000Z';
    mockEvent.picker.endDate = '2017-09-06T22:00:00.000Z';
    component.storeDateRange(mockEvent);
    fixture.detectChanges();

    fixture.whenStable().then(() => { // wait for async getResponsibilities      
      const dateRange: DateRange = sessionContext.getDateRange(Globals.DATE_RANGE_PAST);
      expect(dateRange.dateFrom).toEqual(mockEvent.picker.startDate);
      expect(dateRange.dateTo).toEqual(mockEvent.picker.endDate);
    });
  }));

  it('should get the stored date range from sessioncontext', async(() => {
    spyOn(component, 'setDefaultDateRange').and.callThrough();
    const mockEvent: MockEvent = new MockEvent();    
    mockEvent.picker.startDate = '2017-09-03T22:00:00.000Z';
    mockEvent.picker.endDate = '2017-09-06T22:00:00.000Z';

    const startDate = new Date(mockEvent.picker.startDate);
    const endDate = new Date(mockEvent.picker.endDate);

    component.storeDateRange(mockEvent);
    fixture.detectChanges();    

    fixture.whenStable().then(() => { // wait for async getResponsibilities            
      expect(component.setDefaultDateRange).toHaveBeenCalled();
      expect(component.startDate).toEqual(startDate);
      expect(component.endDate).toEqual(endDate);
    });
  }));
  
});
