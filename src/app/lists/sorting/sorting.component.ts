/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { AfterViewInit, Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { SortingState } from '../../model/sorting-state';

@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html'
})

export class SortingComponent {

  @Input() columnName: string;
  @Input() initColumnName: string;
  @Input() defaultState: boolean;
  @Input() isDesc: boolean;
  
}