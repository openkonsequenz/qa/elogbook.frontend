/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, Input } from '@angular/core';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';
import { Notification } from '../../model/notification';
import { StatusEn, ErrorType } from '../../common/enums';
import { SortingComponent } from '../../lists/sorting/sorting.component';
import * as moment from 'moment';
import { DateRange } from '../../model/date-range';
import { Globals } from '../../common/globals';

@Component({
  selector: 'app-future-notifications',
  templateUrl: './future-notifications.component.html',
  styleUrls: ['./future-notifications.component.css', '../abstract-list/abstract-list.component.css'],
})

export class FutureNotificationsComponent extends AbstractListComponent {
  column: string;

  private getFutureNotifications(): void {
    this.notificationService.getFutureNotifications(this.notificationSearchFilter).subscribe(nots => {
      this.setNotifications(nots);
      this.showSpinner = false;
    },
      error => {
        console.log(error);
        this.messageService.emitError('Zukünftige Meldungen', ErrorType.retrieve); 
      }
    );
    this.showSpinner = true;
  }

  getNotifications(): void {
    this.notificationSearchFilter.dateFrom = moment(this.startDate).toISOString();
    this.notificationSearchFilter.dateTo = moment(this.endDate).toISOString();
    this.getFutureNotifications();
  }

  getHistoricalNotifications(): void {
    this.getFutureNotifications();
  }

  setDefaultDateRange(): void {
    const dateRange: DateRange = this.sessionContext.getDateRange(Globals.DATE_RANGE_FUTURE);
    if (dateRange) {
      this.startDate = new Date(dateRange.dateFrom);
      this.endDate = new Date(dateRange.dateTo);
    } else {
      
      this.startDate = new Date();
      this.startDate.setHours(24, 0, 0, 0);
      this.minDate = this.startDate;

      this.endDate = new Date();
      this.endDate.setMonth(this.endDate.getMonth() + 1);
      this.endDate.setHours(0, 0, 0, 0);
      this.endDate.setHours(48);
    }
  }

  onItemAdded(notification: Notification): void {
    this.getNotifications();
  }

  onItemChanged(notification: Notification): void {
    this.getNotifications();
  }

  storeDateRange(event) {
    const dateRange: DateRange = new DateRange();
    dateRange.dateFrom = event.picker.startDate;
    dateRange.dateTo = event.picker.endDate;
    this.sessionContext.setDateRange(dateRange, Globals.DATE_RANGE_FUTURE);
  }
}
