/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter, SimpleChange } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { click } from '../../testing/index';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { FutureNotificationsComponent } from './future-notifications.component';
import { MockComponent } from '../../testing/mock.component';
import { Notification } from '../../model/notification';
import { FormsModule } from '@angular/forms';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedDatePipe } from '../../common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { SessionContext } from '../../common/session-context';
import { FUTURE_NOTIFICATIONS } from '../../test-data/notifications';
import { DUMMY_NOTIFICATION } from '../../test-data/notifications';
import { DUMMY_CREATED_NOTIFICATION } from '../../test-data/notifications';
import { DUMMY_UPDATED_NOTIFICATION } from '../../test-data/notifications';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { ResponsibilityService } from '../../services/responsibility.service';
import { SortingComponent } from '../../lists/sorting/sorting.component';
import { MessageService } from '../../services/message.service';
import { Globals } from '../../common/globals';
import { DateRange } from '../../model/date-range';
import { LoadingSpinnerComponent } from '../../dialogs/loading-spinner/loading-spinner.component';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { UserService } from 'app/services/user.service';

export class FutureNotificationsMocker {
  public static getComponentMocks() {
    return [
      MockComponent({
        selector: 'app-future-notifications',
        inputs: [
          'responsiblitySelection',
          'withCheckboxes',
          'withEditButtons',
          'isCollapsible',
          'stayHidden',
          'enforceShowReadOnly',
          'gridId',
          'shiftChangeTransactionId',
          'withDatePicker'
        ]
      }),      
    ];

  }
}

describe('FutureNotificationsComponent', () => {
  let component: FutureNotificationsComponent;
  let fixture: ComponentFixture<FutureNotificationsComponent>;

  class MockEvent {
    picker: Picker = new Picker();
  }

  class Picker {
    startDate: string;
    endDate: string;
  }

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getFutureNotifications(notificationType: string) {
      this.loadCalled = true;
      return this;
    };
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    };

    getUserSettings() {
      return this;
    };

    postUserSettings() {
      return this;
    };    
  } 

  let mockService;
  let messageService;
  let sessionContext: SessionContext;
  let mockUserService;

  beforeEach(async(() => {
    mockService = new MockNotificationService();
    messageService = new MessageService();
    sessionContext = new SessionContext();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        FutureNotificationsComponent,
        StringToDatePipe,
        FormattedDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'app-loading-spinner' }),
        SortingComponentMocker.getComponentMocks(),
        MockComponent({ selector: 'input', inputs: ['options'] })
      ],
      providers: [
        { provide: ReminderService, useValue: mockService },
        { provide: MessageService, useValue: messageService },
        { provide: NotificationService, useValue: mockService },
        { provide: SessionContext, useValue: sessionContext },
        { provide: UserService, useValue: mockUserService },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: ResponsibilityService }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FutureNotificationsComponent);
    component = fixture.componentInstance;
  });

  it('should raise edit emitter event when EDIT clicked', async(() => {
    const resps = FUTURE_NOTIFICATIONS;

    mockService.subscribe(() => {
      fixture.detectChanges();
      messageService.matrixFilterChanged$.emit(mockService.content);

      fixture.whenStable().then(() => { // wait for async getFinishedNotifications
        fixture.detectChanges(); // update view with array
        component.onEditNotification.subscribe((notification: Notification) => expect(notification).toBe(resps[0]));
        const des = fixture.debugElement.queryAll(By.css('.btn-primary'));
        click(des[0]);
      });
    });
    mockService.content = resps;
  }));

  it('should retrieve all future notifications', async(() => {
    const resps = FUTURE_NOTIFICATIONS;
 

    mockService.subscribe(() => {
      fixture.detectChanges();
      messageService.matrixFilterChanged$.emit(mockService.content);

      fixture.whenStable().then(() => {
        const des = fixture.debugElement.queryAll(By.css('.btn-primary'));
        expect(des.length).toBe(resps.length);
      });
    });
    mockService.content = resps;
  }));

  it('should call getFutureNotifications after new notification added', async(() => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);
    fixture.whenStable().then(() => { // wait for async getFutureNotifications
      fixture.detectChanges(); // update view with array
      expect(mockService.loadCalled).toBe(true);
    });
  }));

  it('should call getFutureNotifications after notification modified', async(() => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemChanged$.emit(DUMMY_UPDATED_NOTIFICATION);
    fixture.whenStable().then(() => { // wait for async getFutureNotifications
      fixture.detectChanges(); // update view with array

      expect(mockService.loadCalled).toBe(true);
    });
  }));

  it('should run correctly in setError ', async(() => {
    spyOn(console, 'log').and.callThrough();
    const errorMsg = 'Connection Error';
    mockService.error = errorMsg;
    fixture.detectChanges();
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);

    fixture.whenStable().then(() => { // wait for async getResponsibilities
      fixture.detectChanges();        // update view with array
      expect(console.log).toHaveBeenCalledWith(errorMsg);
    });
  }));

  it('should call getHistoricalNotifications on ngOnChanges event', () => {
    spyOn(component, 'getHistoricalNotifications').and.callThrough();
    component.notificationSearchFilter = null;
    component.shiftChangeTransactionId = 396;
    component.ngOnChanges({
      shiftChangeTransactionId: new SimpleChange(null, 396, false)
});

    expect(component.getHistoricalNotifications).toHaveBeenCalled();
    expect(component.notificationSearchFilter.shiftChangeTransactionId)
      .toBe(component.shiftChangeTransactionId);
  });

  it('should store the picked date range in sessioncontext', async(() => {
    const mockEvent: MockEvent = new MockEvent();
    mockEvent.picker.startDate = '2017-09-03T22:00:00.000Z';
    mockEvent.picker.endDate = '2017-09-06T22:00:00.000Z';
    component.storeDateRange(mockEvent);
    fixture.detectChanges();

    fixture.whenStable().then(() => { // wait for async getResponsibilities      
      const dateRange: DateRange = sessionContext.getDateRange(Globals.DATE_RANGE_FUTURE);
      expect(dateRange.dateFrom).toEqual(mockEvent.picker.startDate);
      expect(dateRange.dateTo).toEqual(mockEvent.picker.endDate);
    });
  }));

  it('should get the stored date range from sessioncontext', async(() => {
    spyOn(component, 'setDefaultDateRange').and.callThrough();
    const mockEvent: MockEvent = new MockEvent();    
    mockEvent.picker.startDate = '2017-09-03T22:00:00.000Z';
    mockEvent.picker.endDate = '2017-09-06T22:00:00.000Z';

    const startDate = new Date(mockEvent.picker.startDate);
    const endDate = new Date(mockEvent.picker.endDate);

    component.storeDateRange(mockEvent);
    fixture.detectChanges();    

    fixture.whenStable().then(() => { // wait for async getResponsibilities            
      expect(component.setDefaultDateRange).toHaveBeenCalled();
      expect(component.startDate).toEqual(startDate);
      expect(component.endDate).toEqual(endDate);
    });
  }));


});
