/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter, SimpleChange } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { click } from '../../testing/index';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { AbstractListComponent } from './abstract-list.component';
import { Notification } from '../../model/notification';
import { FormsModule } from '@angular/forms';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { FormattedDatePipe } from '../../common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { SessionContext } from '../../common/session-context';
import { FINISHED_NOTIFICATIONS } from '../../test-data/notifications';
import { DUMMY_NOTIFICATION } from '../../test-data/notifications';
import { DUMMY_CREATED_NOTIFICATION } from '../../test-data/notifications';
import { DUMMY_UPDATED_NOTIFICATION } from '../../test-data/notifications';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MockComponent } from '../../testing/mock.component';
import { NotificationSearchFilter } from '../../model/notification-search-filter';
import { ResponsibilityService } from '../../services/responsibility.service';
import { MessageService } from '../../services/message.service';
import { UserService } from 'app/services/user.service';


export class AbstractListMocker {
  public static getComponentMocks() {
    return [
        MockComponent({
          selector: 'app-abstract-list', 
          inputs: [ 'withCheckboxes', 'withEditButtons', 'isCollapsible', 'stayHidden', 'gridId', 'enforceShowReadOnly']
        })      
    ];
  }
}
describe('AbstractListComponent', () => {
  let component: AbstractListComponent;
  let fixture: ComponentFixture<AbstractListComponent>;
  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;

    public getFinishedNotifications(notificationSearchFilter: NotificationSearchFilter) {
      this.loadCalled = true;
      return this;
    };

    public getNotificationVersions(notification: Notification) {
      return this;
    }
  }
  
  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    };

    getUserSettings() {
      return this;
    };

    postUserSettings() {
      return this;
    };    
  } 

  class MockReminderService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getCurrentReminders() { // notificationSearchFilter: NotificationSearchFilter
      this.loadCalled = true;
      return this;
    };
  }

  let sessionContext;
  let mockReminderService;
  let mockService;
  let messageService;
  let mockUserService;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockService = new MockNotificationService();
    mockReminderService = new MockReminderService();
    messageService = new MessageService();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        AbstractListComponent,
        StringToDatePipe,
        FormattedDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'input', inputs: ['options'] })
      ],
      providers: [
        { provide: MessageService, useValue: messageService },
        { provide: NotificationService, useValue: mockService },
        { provide: ReminderService, useValue: mockReminderService },
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractListComponent);
    component = fixture.componentInstance;    
  });

  it('should call getNotifications after new notification added', async(() => {
    spyOn(component, 'onItemAdded');
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);
    fixture.whenStable().then(() => { // wait for async getNotifications
      fixture.detectChanges(); // update view with array
      expect(component.onItemAdded).toHaveBeenCalledWith(DUMMY_CREATED_NOTIFICATION);
    });
  }));


  it('should call getNotifications after new notification added', async(() => {
    spyOn(component, 'onItemAdded');
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);
    fixture.whenStable().then(() => { // wait for async getNotifications
      fixture.detectChanges(); // update view with array
      expect(component.onItemAdded).toHaveBeenCalledWith(DUMMY_CREATED_NOTIFICATION);
    });
  }));

  it('should call getNotificationVersions on retrieveNotificationVersions', async(() => {
    mockService.content = FINISHED_NOTIFICATIONS;

    // 3 toggle = 1 toggle
    component.toggleHistoryTab(DUMMY_NOTIFICATION);
    component.toggleHistoryTab(DUMMY_NOTIFICATION);
    component.toggleHistoryTab(DUMMY_NOTIFICATION);
    fixture.detectChanges();
    fixture.whenStable().then(() => { // wait for async getNotifications
      fixture.detectChanges(); // update view with array
      expect(component.getNotificationVersions(DUMMY_NOTIFICATION).length).toBe(FINISHED_NOTIFICATIONS.length);
      // clean up
      DUMMY_NOTIFICATION.decoratorNotificationVersions = null;
    });
  }));

  it('should set error on retrieveNotificationVersions failure', async(() => {
    let hasBeenCalled = false;
    messageService.errorOccured$.subscribe( msg => hasBeenCalled = true);

    mockService.error = 'VERSION_ERROR';
    component.retrieveNotificationVersions(DUMMY_NOTIFICATION);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(hasBeenCalled).toBeTruthy();

    });
  }));

  it('should call getHistoricalNotifications after change of shiftChangeTransactionId', async(() => {
    spyOn(component, 'getHistoricalNotifications');
    component.notificationSearchFilter = null;
    component.shiftChangeTransactionId = 666;
    const compUnconvered: any = component;
    compUnconvered.onShiftChangeTransactionChanged();

    expect(component.getHistoricalNotifications).toHaveBeenCalled();
    expect(component.notificationSearchFilter.shiftChangeTransactionId)
      .toBe(component.shiftChangeTransactionId);
  }));

  it('should send "onLookUpNotification"', async(() => {
    const testNotification = DUMMY_CREATED_NOTIFICATION;
    let notId = -1;
    component.onLookUpNotification.subscribe(
      not => notId = not.id);
    component.lookUpNotification(testNotification);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(notId).toBe(testNotification.id);
    });
  }));

  it('should call changeAllSelection correctly', () => {
    component.notifications = FINISHED_NOTIFICATIONS;
    component.notifications[1].selected = false;
    component.selectAll = true;
    component.changeAllSelection();
    expect(component.notifications[1].selected).toBe(true);

    component.selectAll = false;
    component.changeAllSelection();
    expect(component.notifications[1].selected).toBe(false);
  });

  it('should call selectChanged correctly', () => {
    // first call empty
    component.notifications = null;
    component.selectionChanged();
    component.selectAll = false;
    component.notifications = FINISHED_NOTIFICATIONS;
    component.notifications[0].selected = true;
    component.notifications[1].selected = true;
    component.notifications[2].selected = true;
    component.selectionChanged();
    expect(component.selectAll).toBe(true);
    component.notifications[1].selected = false;
    component.selectionChanged();
    expect(component.selectAll).toBe(false);
  });

  it('should call getHistoricalNotifications on ngOnChanges event', () => {    
    spyOn(component, 'getHistoricalNotifications').and.callThrough();
    component.notificationSearchFilter = null;    
    component.shiftChangeTransactionId = 396;
    component.ngOnChanges({
      shiftChangeTransactionId: new SimpleChange(null, 396, false)
    });
   
    expect(component.getHistoricalNotifications).toHaveBeenCalled();
    expect(component.notificationSearchFilter.shiftChangeTransactionId)
      .toBe(component.shiftChangeTransactionId);
  });

  
  it('should return a new ListHelperTool', () => {
    expect(component.getListHelperTool()).toBeTruthy();    
  });

});
