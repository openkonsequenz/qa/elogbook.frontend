/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MdDialogModule } from '@angular/material';
import { Daterangepicker } from 'ng2-daterangepicker';
import { HttpModule } from '@angular/http';
import { NotificationService } from './services/notification.service';
import { AuthenticationService } from './services/authentication.service';
import { BaseDataService } from './services/base-data.service';
import { ResponsibilityService } from './services/responsibility.service';
import { SearchResultService } from './services/search-result.service';
import { UserService } from './services/user.service';
import { SessionContext } from './common/session-context';
import { VersionInfoService } from './services/version-info.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OverviewComponent } from './pages/overview/overview.component';
import { FutureNotificationsComponent } from './lists/future-notifications/future-notifications.component';
import { OpenNotificationsComponent } from './lists/open-notifications/open-notifications.component';
import { FinishedNotificationsComponent } from './lists/finished-notifications/finished-notifications.component';
import { SearchResultListComponent } from './lists/search-result-list/search-result-list.component';
import { SearchComponent } from './pages/search/search.component';
import { EntryComponent } from './dialogs/entry/entry.component';
import { ShiftChangeComponent } from './dialogs/shift-change/shift-change.component';
import { ResponsibilityComponent } from './dialogs/responsibility/responsibility.component';
import { ShiftChangeProtocolComponent } from './dialogs/shift-change-protocol/shift-change-protocol.component';
import { LogoutComponent } from './dialogs/logout/logout.component';
import { VersionInfoComponent } from './common-components/version-info/version-info.component';
import { StringToDatePipe } from './common-components/pipes/string-to-date.pipe';
import { FormattedDatePipe } from './common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from './common-components/pipes/formatted-timestamp.pipe';
import { AbstractListComponent } from './lists/abstract-list/abstract-list.component';
import { AngularMultiSelectComponent } from './common-components/multiselect-dropdown/multiselect.component';
import { ClickOutsideDirective } from './common-components/multiselect-dropdown/clickOutside';
import { ListFilterPipe } from './common-components/multiselect-dropdown/list-filter';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Injectable } from '@angular/core';
import { BaseRequestOptions, Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { AlertComponent } from './dialogs/alert/alert.component';
import { FilterComponent } from './filter/filter.component';
import { AutocompleteComponent } from './common-components/autocomplete/autocomplete.component';
import { MainNavigationComponent } from './common-components/main-navigation/main-navigation.component';
import { ShiftChangeOverviewComponent } from './pages/shift-change-overview/shift-change-overview.component';
import { ReminderComponent } from './pages/reminder/reminder.component';
import { HistoricalShiftChangesComponent } from './lists/historical-shift-changes/historical-shift-changes.component';
import { ReminderService } from './services/reminder.service';
import { MessageService } from './services/message.service';
import { BaseDataLoaderService } from './services/jobs/base-data-loader.service';
import { CurrentRemindersComponent } from './lists/current-reminders/current-reminders.component';
import { ReminderCallerJobService } from './services/jobs/reminder-caller-job.service';
import { ImportFileCallerService } from './services/jobs/import-file-caller.service';
import { LoadingSpinnerComponent } from './dialogs/loading-spinner/loading-spinner.component';
import { SortingComponent } from './lists/sorting/sorting.component';
import { ImportService } from './services/import.service';
import { FileImportComponent } from './dialogs/file-import/file-import.component';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { MessageBannerComponent } from './common-components/message-banner/message-banner.component';
import { LogoutPageComponent } from './pages/logout/logout.component';
import { RemoveButtonComponent } from './common-components/remove-button/remove-button.component';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    FutureNotificationsComponent,
    OpenNotificationsComponent,
    FinishedNotificationsComponent,
    SearchResultListComponent,
    ResponsibilityComponent,
    EntryComponent,
    ShiftChangeComponent,
    SearchComponent,
    ShiftChangeProtocolComponent,
    LogoutComponent,
    AlertComponent,
    VersionInfoComponent,
    StringToDatePipe,
    FormattedDatePipe,
    FormattedTimestampPipe,
    AbstractListComponent,
    AngularMultiSelectComponent,
    ClickOutsideDirective,
    ListFilterPipe,
    AutocompleteComponent,
    MainNavigationComponent,
    ReminderComponent,
    ShiftChangeOverviewComponent,
    HistoricalShiftChangesComponent,
    CurrentRemindersComponent,
    FilterComponent,
    LoadingSpinnerComponent,
    SortingComponent,
    FileImportComponent,
    MessageBannerComponent,
    LogoutPageComponent,
    RemoveButtonComponent      
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    Daterangepicker,
    HttpModule,
    MdDialogModule.forRoot()
  ],
  entryComponents: [
    ResponsibilityComponent,
    EntryComponent,
    FileImportComponent,
    SearchComponent,
    ShiftChangeComponent,
    ShiftChangeProtocolComponent,
    LogoutComponent,
    LoadingSpinnerComponent,
    AlertComponent
  ],
  providers: [
    NotificationService,
    AuthenticationService,
    ResponsibilityService,
    ReminderService,
    SearchResultService,
    VersionInfoService,
    UserService,
    SessionContext,
    BaseDataService,
    ReminderCallerJobService,
    ImportFileCallerService,
    MessageService,
    BaseDataLoaderService,
    ImportService,
    HttpResponseInterceptorService  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
