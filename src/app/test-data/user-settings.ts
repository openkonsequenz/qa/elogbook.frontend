/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { Responsibility } from '../model/responsibility';
import { HistoricalShiftChanges } from '../model/historical-shift-changes';
import { HistoricalResponsibility } from '../model/historical-responsibility';
import { UserSettings } from 'app/model/user-settings';
import { SortingState } from 'app/model/sorting-state';
export const USERSETTING_1: UserSettings = {


    activeFilterRespIds: [
        27,
        25,
        29,
        31,
        32
      ],
    sortingStateMap: new Map([
            [ "OverviewOpenNot", 
                {
                "column": "fkRefNotificationStatus",
                "isDesc": true,
                "counter": 1,
                "defaultState": false
                }
            ]        
        ])
};
