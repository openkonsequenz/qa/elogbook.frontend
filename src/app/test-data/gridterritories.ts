/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { GridTerritory } from '../model/gridterritory';

export const GRIDTERRITORIES: GridTerritory[] = [
    {'id': 1, 'name': 'MA', 'description': 'Mannheim', 'fkRefMaster': 1},
    {'id': 2, 'name': 'OF1', 'description': 'Offenbach 1', 'fkRefMaster': 2},
    {'id': 3, 'name': 'OF2', 'description': 'Offenbach 2', 'fkRefMaster': 2},
    {'id': 4, 'name': 'KS', 'description': 'Kassel', 'fkRefMaster': 4}
];
