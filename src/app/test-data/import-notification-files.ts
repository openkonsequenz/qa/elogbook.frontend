/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { NotificationFileModel } from '../model/file-model';

export const EMPTY_IMPORT_NOTIFICATION_FILES: NotificationFileModel[] = [

];

export const IMPORT_NOTIFICATION_FILES: NotificationFileModel[] = [
    {
        fileName: '20170918_otto_ma_strom_stoe1.csv',
        creationDate: '2017-09-18T06:30:00+01',
        creator: 'otto',
        type: 'csv',
        size: 2,

        data: 'Störung Mannheim Hauptleitung',

        branchName: 'S',
        gridTerritoryName: 'MA',
        notificationText: 'Störung Mannheim Hauptleitung'
    },
    {
        fileName: '20170920_max_of_gas_stoe2.csv',
        creationDate: '2017-09-20T16:30:00+01',
        creator: 'max',
        type: 'csv',
        size: 2,

        data: 'Störung Offenbach Gasgeruch',

        branchName: 'G',
        gridTerritoryName: 'OF',
        notificationText: 'Störung Offenbach Gasgeruch'
    }
];

export const ONE_IMPORT_NOTIFICATION_FILE: NotificationFileModel[] = [
    {
        fileName: '20170918_otto_ma_strom_stoe1.csv',
        creationDate: '2017-09-18T06:30:00+01',
        creator: 'otto',
        type: 'csv',
        size: 2,

        data: 'Störung Mannheim Hauptleitung',

        branchName: 'S',
        gridTerritoryName: 'MA',
        notificationText: 'Störung Mannheim Hauptleitung'
    }
];