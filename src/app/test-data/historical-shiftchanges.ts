/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { Responsibility } from '../model/responsibility';
import { HistoricalShiftChanges } from '../model/historical-shift-changes';
import { HistoricalResponsibility } from '../model/historical-responsibility';
export const HISTORICAL_SCHIFTCHANGES: HistoricalShiftChanges = {

        startDate: '2017-08-11T18:58:39.841Z',
        endDate: '2017-08-18T18:58:39.841Z',
        historicalResponsibilities: [
            new HistoricalResponsibility ({
                responsibleUser: 'max',
                formerResponsibleUser: 'otto',
                transferDate: '2017-08-18T15:00:21.963Z',
                transactionId: 23
            }),

            new HistoricalResponsibility ({
                responsibleUser: 'max',
                formerResponsibleUser: 'pete',
                transferDate: '2017-08-18T15:00:21.963Z',
                transactionId: 23
            }),

            new HistoricalResponsibility ({
                responsibleUser: 'pete',
                formerResponsibleUser: 'otto',
                transferDate: '2017-08-18T14:19:56.093Z',
                transactionId: 22
            })
        ]
    };
