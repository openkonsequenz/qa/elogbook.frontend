/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { FilterMatrix } from '../model/controller-model/filter-matrix';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { Responsibility } from '../model/responsibility';

export const FILTER_MATRIX_NONE_SELECTED: FilterMatrix = {
    responsibilityContainerMatrix: <TerritoryResponsibility[]>[
        {
            id: 1,
            gridTerritoryDescription: 'Mannheim',
            responsibilityList: [

                new Responsibility({
                    id: 1,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 2,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 3,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 4,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                })
            ]
        },
        {
            id: 2,
            gridTerritoryDescription: 'Offenbach',
            responsibilityList: [
                new Responsibility({
                    id: 5,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 6,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 7,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 8,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                })
            ]
        },
        {
            id: 3,
            gridTerritoryDescription: 'Kassel',
            responsibilityList: [
                new Responsibility({
                    id: 9,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 10,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 11,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 12,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                }),
            ]
        }
    ],

    getNumFilterList(): number[] { return []; }
};

export const FILTER_MATRIX_ALL_SELECTED: FilterMatrix = {
    responsibilityContainerMatrix: <TerritoryResponsibility[]>[
        {
            id: 1,
            gridTerritoryDescription: 'Mannheim',
            responsibilityList: [

                new Responsibility({
                    id: 1,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: true
                }),
                new Responsibility({
                    id: 2,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: true
                }),
                new Responsibility({
                    id: 3,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: true
                }),
                new Responsibility({
                    id: 4,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: true
                })
            ]
        },
        {
            id: 2,
            gridTerritoryDescription: 'Offenbach',
            responsibilityList: [
                new Responsibility({
                    id: 5,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: true
                }),
                new Responsibility({
                    id: 6,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: true
                }),
                new Responsibility({
                    id: 7,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: true
                }),
                new Responsibility({
                    id: 8,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: true
                })
            ]
        },
        {
            id: 3,
            gridTerritoryDescription: 'Kassel',
            responsibilityList: [
                new Responsibility({
                    id: 9,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: true
                }),
                new Responsibility({
                    id: 10,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: true
                }),
                new Responsibility({
                    id: 11,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: true
                }),
                new Responsibility({
                    id: 12,
                    responsibleUser: 'User1',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: true
                }),
            ]
        }
    ],

    getNumFilterList(): number[] {
        return [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
        ];
    }
};

export const FILTER_MATRIX_UNIQUE_BRANCH: FilterMatrix = {
    responsibilityContainerMatrix: <TerritoryResponsibility[]>[
        {
            id: 1,
            gridTerritoryDescription: 'Mannheim',
            responsibilityList: [

                new Responsibility({
                    id: 1,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 2,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 3,
                    responsibleUser: 'pete',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 4,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                })
            ]
        },
        {
            id: 2,
            gridTerritoryDescription: 'Offenbach',
            responsibilityList: [
                new Responsibility({
                    id: 5,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 6,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 7,
                    responsibleUser: 'pete',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 8,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                })
            ]
        },
        {
            id: 3,
            gridTerritoryDescription: 'Kassel',
            responsibilityList: [
                new Responsibility({
                    id: 9,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 10,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 11,
                    responsibleUser: 'pete',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 12,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                }),
            ]
        }
    ],

    getNumFilterList(): number[] { return []; }
};

export const FILTER_MATRIX_UNIQUE_GRIDTERRITORY: FilterMatrix = {
    responsibilityContainerMatrix: <TerritoryResponsibility[]>[
        {
            id: 1,
            gridTerritoryDescription: 'Mannheim',
            responsibilityList: [

                new Responsibility({
                    id: 1,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 2,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 3,
                    responsibleUser: 'max',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 4,
                    responsibleUser: 'admin',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                })
            ]
        },
        {
            id: 2,
            gridTerritoryDescription: 'Offenbach',
            responsibilityList: [
                new Responsibility({
                    id: 5,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 6,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 7,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 8,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                })
            ]
        },
        {
            id: 3,
            gridTerritoryDescription: 'Offenbach 2',
            responsibilityList: [
                new Responsibility({
                    id: 9,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 10,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 11,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 12,
                    responsibleUser: 'otto',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                }),
            ]
        },
        {
            id: 4,
            gridTerritoryDescription: 'Kassel',
            responsibilityList: [
                new Responsibility({
                    id: 13,
                    responsibleUser: 'pete',
                    newResponsibleUser: '',
                    branchName: 'S',
                    isActive: false
                }),
                new Responsibility({
                    id: 14,
                    responsibleUser: 'pete',
                    newResponsibleUser: '',
                    branchName: 'W',
                    isActive: false
                }),
                new Responsibility({
                    id: 15,
                    responsibleUser: 'pete',
                    newResponsibleUser: '',
                    branchName: 'F',
                    isActive: false
                }),
                new Responsibility({
                    id: 16,
                    responsibleUser: 'pete',
                    newResponsibleUser: '',
                    branchName: 'G',
                    isActive: false
                }),
            ]
        }
    ],

    getNumFilterList(): number[] { return []; }
};