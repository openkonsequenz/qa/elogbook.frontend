/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { BannerMessageStatusEn } from '../../common/enums';
import { SessionContext } from '../../common/session-context';

@Component({
  selector: 'app-message-banner',
  templateUrl: './message-banner.component.html',
  styleUrls: ['./message-banner.component.css']
})


export class MessageBannerComponent implements OnInit, OnDestroy {
  static allComponents: Map< MessageBannerComponent, number > = new Map<MessageBannerComponent, number>();
  @Input() zOrder = 0;


  
  bannerMessageStatus = BannerMessageStatusEn;
  

  constructor(public sessionContext: SessionContext) { 
  }

  ngOnInit() {
    MessageBannerComponent.allComponents.set(this, this.zOrder );    
  }

  ngOnDestroy() {
    MessageBannerComponent.allComponents.delete(this);
  }

  getMaxMessageBannerZOrder(): number {
    const iter =  MessageBannerComponent.allComponents.values();
    let maxValue = 0;
    let item = iter.next();
    while ( item != null && !item.done ) {
      if ( item.value > maxValue ) {
        maxValue = item.value;
      }
      item = iter.next();
    }

    return maxValue;
  }

}
