/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SessionContext } from '../../common/session-context';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { MessageBannerComponent } from './message-banner.component';
import { BannerMessage } from '../../common/banner-message';
import { BannerMessageStatusEn } from '../../common/enums';

describe('MessageBannerComponent', () => {
  let component: MessageBannerComponent;
  let fixture: ComponentFixture<MessageBannerComponent>;
  let sessionContext: SessionContext;  

  beforeEach(async(() => {
    sessionContext = new SessionContext();  

    TestBed.configureTestingModule({
      declarations: [ MessageBannerComponent ],
      providers: [
        { provide: SessionContext, useValue: sessionContext },
      ],  
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
     expect(component).toBeTruthy();
   });

  it('should count the zOrder correctly and toggle the visibility due to zOrder', () => {
    const fixture2 = TestBed.createComponent(MessageBannerComponent);
    const component2: any = fixture2.componentInstance;
    const bannerMessage = new BannerMessage();

    sessionContext.setBannerMessage(BannerMessageStatusEn.success, 'Testor', true);

    const x1: any = component;
    x1.ngOnInit();

    expect(component.getMaxMessageBannerZOrder()).toBe(0);
    fixture.detectChanges();

    // Component 1 is the only one and has the topmost zOrder=>Visible
    expect(fixture.debugElement.query( By.css('.close'))).not.toBeNull();
    
    component2.zOrder = 666;
    component2.ngOnInit();

    expect(component.getMaxMessageBannerZOrder()).toBe(666);
    fixture.detectChanges();
    fixture2.detectChanges();

    // Component 2 has a bigger zOrder => Component1=invisible, Component2=visible
    expect(fixture.debugElement.query( By.css('.close'))).toBeNull();
    expect(fixture2.debugElement.query( By.css('.close'))).not.toBeNull();

  });
});
