/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'formattedTimestamp'
})
export class FormattedTimestampPipe implements PipeTransform {
    transform(value: any, format: string = ''): string {
        // Try and parse the passed value.
        const momentDate = moment(value);

        if ( value === '' ) {
            return '';
        }

        if ( value === 'xxx') {
            return null;
        }

        if ( value === null ) {
            return '';
        }

        if ( value === undefined ) {
            return '';
        }

        // If moment didn't understand the value, return it unformatted.
        if (!momentDate.isValid()) {
            return value;
        }
        // Otherwise, return the date formatted as requested.
        return momentDate.format(format);
    }
}
