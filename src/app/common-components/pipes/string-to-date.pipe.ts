/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';
/**
 * Pipe to transform a string to a date
 */
@Pipe({name: 'stringToDate'})
export class StringToDatePipe implements PipeTransform {
    /**
     * Constructor
     */
    constructor() {
    }
    /**
     * Transform a date that is passed as string into a date
     * @param value The date passed as string
     * @returns {Date} The Date object
     */
    transform(value: string, format: string = ''): Date {
        if ( value === null || value === undefined || value === ''  ) {
            return null;
        }
        const ret = new Date(value);
        if ( ret + '' === 'Invalid Date') {
            return null;
        } else {
            return new Date( ret );
        }
    }
}
