/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed } from '@angular/core/testing';
import { FormattedTimestampPipe } from './formatted-timestamp.pipe';

describe('Test FormattedTimestampPipe ', () => {
    const pipe = new  FormattedTimestampPipe();

  it('should perform correct with different inputs ', () => {
      expect( pipe.transform('xxx') ).toBeNull();
      expect( pipe.transform( null ) ).toBe('');
      expect( pipe.transform( '' ) ).toBe('');
      expect( pipe.transform( undefined ) ).toBe('');
      
      const dateTest = pipe.transform( '19.11.2017 13:16');
      expect( dateTest.match('[0-3][0-9]\.[0-1][0-9]\.[0-9]{4} [0-2][0-9]:[0-5][0-9]') ).toBeTruthy() ;
      
  });
});
