/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed } from '@angular/core/testing';
import { StringToDatePipe } from './string-to-date.pipe';

describe('Test StringToDatePipe ', () => {
    const pipe = new  StringToDatePipe();

  it('should perform correct with different inputs ', () => {
      expect( pipe.transform('xxx') ).toBeNull();
      expect( pipe.transform( null) ).toBeNull();
      expect( pipe.transform( '' ) ).toBeNull();
      expect( pipe.transform( undefined ) ).toBeNull();
      
      const dateTest = pipe.transform( '2017-11-19T13:16:06');
      expect( dateTest.getFullYear() ).toBe( 2017 );
      expect( dateTest.getMonth() ).toBe( 10 ); // month in jscript is zero based!
      expect( dateTest.getDate() ).toBe( 19 );
      expect( dateTest.getMinutes() ).toBe( 16 );
      expect( dateTest.getSeconds() ).toBe( 6 );
  });
});
