/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';
/**
 * Pipe to transform a string to a date
 */
@Pipe({name: 'formattedDate'})
export class FormattedDatePipe implements PipeTransform {
    /**
     * Constructor
     */
    constructor() {
    }
    /**
     * Transform a date that is passed as string into a date
     * @param value The date passed as string
     * @param format The date passed as string
     */
   transform(value: any, format: string = ''): string {
        // Try and parse the passed value.
        const momentDate = moment(value);

        // If moment didn't understand the value, return it unformatted.
        if (!momentDate.isValid()) { 
            return value;
        }

        // Otherwise, return the date formatted as requested.
        if (format) {
            return momentDate.format(format);
        }
        return momentDate.format('DD.MM.YYYY');
    }
}
