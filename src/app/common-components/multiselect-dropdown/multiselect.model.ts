/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
export class ListItem {
    id: number;
    itemName: string;
}
export class MyException {
	 status: number;
	 body: any;
	 
     constructor(status: number, body: any) {
		this.status = status;
		this.body = body;
	}
	
}