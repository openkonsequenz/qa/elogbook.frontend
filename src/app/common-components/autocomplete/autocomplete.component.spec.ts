/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { AutocompleteComponent } from './autocomplete.component';
import { NotificationService } from '../../services/notification.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { click, focus, newEvent, pressKey } from '../../testing/index';
import { MessageService } from '../../services/message.service';
import { CONTACTTUPEL } from 'app/test-data/contact-tupel';
import { DUMMY_NOTIFICATION } from 'app/test-data/notifications';

describe('AutocompleteComponent', () => {
  let component: AutocompleteComponent;
  let fixture: ComponentFixture<AutocompleteComponent>;
  let de: DebugElement;  // the DebugElement with the welcome message  
  let inputElement: HTMLInputElement;

  class MockBtbService extends AbstractMockObservableService {
    getAssignedUserSuggestions() {
      return this;
    }
  }
  let mockService;
  let messageService;

  beforeEach(async(() => {
    mockService = new MockBtbService();
    messageService = new MessageService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [AutocompleteComponent],
      providers: [
        { provide: NotificationService, useValue: mockService }, 
        { provide: MessageService, useValue: messageService }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteComponent);
    component = fixture.componentInstance;
  });

  it('test1: inputfield should be empty at beginning', () => {
    de = fixture.debugElement.query(By.css('.entry-input'));
    inputElement = de.nativeElement;

    const targetString = '';
    expect(inputElement.textContent).toBe(targetString);
  });

  it('test2: should call setAssignedUserSuggestion on focus', async(() => {
    component.notification = DUMMY_NOTIFICATION;
    const userSuggestionsList = CONTACTTUPEL;
    mockService.content = userSuggestionsList;  
    

    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.entry-input'));
    inputElement = de.nativeElement;

    focus(de);
    fixture.detectChanges();
    expect(component.alreadyFocused).toBe(true);

    fixture.whenStable().then(() => {
      expect(component.inputElementList.length).toBe(5);
      //items have to have the same length as inputElementList
      expect(component.items.length).toBe(5);
    });

  }));

  it('test2.1: should call setAssignedUserSuggestion on focus and return an error', async(() => {
    spyOn(component, 'createItemList').and.callThrough();
    component.notification = DUMMY_NOTIFICATION;

    const userSuggestionsList = [];
    mockService.error = 'MOCKERROR';
    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.entry-input'));
    inputElement = de.nativeElement;

    focus(de);
    fixture.detectChanges();
    expect(component.alreadyFocused).toBe(true);

    fixture.whenStable().then(() => {
      expect(component.createItemList).not.toHaveBeenCalled();
      expect(component.inputElementList.length).toBe(0);
    });

  }));

  it('test3: should filter all usersnames containing "test"', fakeAsync(() => {
    //const userSuggestionsList = ['User1', 'TestUser1', 'TestUser2', 'User2', 'User3'];
    component.notification = DUMMY_NOTIFICATION;
    const userSuggestionsList = CONTACTTUPEL;
    mockService.content = userSuggestionsList;

    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.entry-input'));
    inputElement = de.nativeElement;

    focus(de);
    tick();
    fixture.detectChanges();

    expect(component.alreadyFocused).toBe(true);

    inputElement.value = 'test';
    inputElement.dispatchEvent(newEvent('input'));
    fixture.detectChanges();

    expect(component.notification.responsibilityForwarding).toBe('test');

    //keycode 56 = 'a' : random keycode just to satisfy the filter method 
    // the actual value stays untouched
    pressKey(de, 'keyup', 56);

    tick();
    fixture.detectChanges();

    expect(component.filteredList.length).toBe(2);
    expect(component.filteredList[0].name).toBe('TestUser1');

  }));

  it('test4: should select 2nd item in list after pressing "Arrow-key Down" 2 times and then "Enter"', fakeAsync(() => {
    //const userSuggestionsList = ['User1', 'TestUser1', 'TestUser2', 'User2', 'User3'];
    component.notification = DUMMY_NOTIFICATION;
    const userSuggestionsList = CONTACTTUPEL;
    mockService.content = userSuggestionsList;

    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.entry-input'));
    inputElement = de.nativeElement;

    focus(de);
    tick();
    fixture.detectChanges();

    component.filterQuery();
    component.notification.responsibilityForwarding = 'user';
    tick();
    fixture.detectChanges();

    //keyup event NOT the key "keyUp"
    //keycode 40 = "down arrow"
    pressKey(de, 'keyup', 40);
    fixture.detectChanges();
    //keycode 40 = "down arrow"
    pressKey(de, 'keyup', 40);
    fixture.detectChanges();
    //keycode 13 = "enter"
    pressKey(de, 'keyup', 13);
    tick();
    fixture.detectChanges();

    //selected user 2x down arrow & 1x enter
    expect(component.notification.responsibilityForwarding).toBe('TestUser2');

  }));

  it('test4.1 : should select 1st item in list after pressing "Arrow-key Down" 2x and\
      "Arrow-key Up" again and then "Enter"', fakeAsync(() => {
      //const userSuggestionsList = ['User1', 'TestUser1', 'TestUser2', 'User2', 'User3'];
      component.notification = DUMMY_NOTIFICATION;
      const userSuggestionsList = CONTACTTUPEL;
      mockService.content = userSuggestionsList;

      fixture.detectChanges();

      de = fixture.debugElement.query(By.css('.entry-input'));
      inputElement = de.nativeElement;

      focus(de);
      tick();
      fixture.detectChanges();

      component.filterQuery();
      component.notification.responsibilityForwarding = 'user';
      tick();
      fixture.detectChanges();

      //keyup event NOT the key "keyUp"
      //keycode 40 = "down arrow"
      pressKey(de, 'keyup', 40);
      fixture.detectChanges();
      pressKey(de, 'keyup', 40);
      fixture.detectChanges();
      //keycode 40 = "down arrow"
      pressKey(de, 'keyup', 38);
      fixture.detectChanges();
      //keycode 13 = "enter"
      pressKey(de, 'keyup', 13);
      tick();
      fixture.detectChanges();

      //selected user 2x down arrow & 1x enter
      expect(component.notification.responsibilityForwarding).toBe('TestUser2');

    }));

  it('test5: should call handleClick', () => {
    spyOn(component, 'handleClick').and.callThrough();
    component.notification = DUMMY_NOTIFICATION;    

    const keyEventObj = new Event('click');
    Object.defineProperty(keyEventObj, 'target', { 'value': component.elementRef.nativeElement });
    document.dispatchEvent(keyEventObj);

    expect(component.handleClick).toHaveBeenCalledTimes(1);
    document.dispatchEvent(newEvent('click'));
    expect(component.handleClick).toHaveBeenCalledTimes(2);
  });

  it('test6: should call handleKeyboardEvent', () => {
    spyOn(component, 'handleKeyboardEvent').and.callThrough();
    component.notification = DUMMY_NOTIFICATION;

    const keyEventObj = new Event('keypress');
    Object.defineProperty(keyEventObj, 'keyCode', { 'value': 13 });
    Object.defineProperty(keyEventObj, 'target', { 'value': component.elementRef.nativeElement });
    document.dispatchEvent(keyEventObj);

    expect(component.handleKeyboardEvent).toHaveBeenCalledTimes(1);
    document.dispatchEvent(newEvent('keypress'));
    expect(component.handleKeyboardEvent).toHaveBeenCalledTimes(2);
  });

  it('test6.1: should call handleKeyboardEvent', fakeAsync(() => {
    spyOn(component, 'handleKeyboardEvent').and.callThrough();
    //const userSuggestionsList = ['User1', 'TestUser1', 'TestUser2', 'User2', 'User3'];
    component.notification = DUMMY_NOTIFICATION;
    const userSuggestionsList = CONTACTTUPEL;
    mockService.content = userSuggestionsList;
    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.entry-input'));
    inputElement = de.nativeElement;

    focus(de);
    tick();
    fixture.detectChanges();

    component.filterQuery();
    component.notification.responsibilityForwarding = 'user';
    tick();
    fixture.detectChanges();

    //keyup event NOT the key "keyUp"
    //keycode 40 = "down arrow"
    pressKey(de, 'keyup', 40);
    fixture.detectChanges();
    tick();

    const keyEventObj = new Event('keypress');
    Object.defineProperty(keyEventObj, 'keyCode', { 'value': 13 });
    Object.defineProperty(keyEventObj, 'target', { 'value': component.elementRef.nativeElement });
    document.dispatchEvent(keyEventObj);
    tick();

    expect(component.handleKeyboardEvent).toHaveBeenCalledTimes(1);
    expect(component.notification.responsibilityForwarding).toBe(userSuggestionsList[2].contactName);
    expect(component.filteredList.length).toBe(0);
    expect(component.position).toBe(-1);
  }));

  it('test7: should call handleKeyDown', () => {
    spyOn(component, 'handleKeyDown').and.callThrough();
    component.notification = DUMMY_NOTIFICATION;    

    const keyEventObj = new Event('keydown');
    Object.defineProperty(keyEventObj, 'keyCode', { 'value': 40 });
    component.elementRef.nativeElement.dispatchEvent(keyEventObj);
    expect(component.handleKeyDown).toHaveBeenCalledTimes(1);

    Object.defineProperty(keyEventObj, 'keyCode', { 'value': 38 });
    component.elementRef.nativeElement.dispatchEvent(keyEventObj);
    expect(component.handleKeyDown).toHaveBeenCalledTimes(2);

    component.elementRef.nativeElement.dispatchEvent(newEvent('keydown'));
    expect(component.handleKeyDown).toHaveBeenCalledTimes(3);
  });

  it('test8: should clear the suggestions when responsibilityForwarding is empty', fakeAsync(() => {
    component.notification = DUMMY_NOTIFICATION;
    const userSuggestionsList = CONTACTTUPEL;
    //const userSuggestionsList = ['User1', 'TestUser1', 'TestUser2', 'User2', 'User3'];
    mockService.content = userSuggestionsList;
    

    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.entry-input'));
    inputElement = de.nativeElement;

    focus(de);
    tick();
    fixture.detectChanges();
    component.notification.responsibilityForwarding = '';
    pressKey(de, 'keyup', 47);
    tick();
    fixture.detectChanges();

    expect(component.filteredList.length).toBe(0);

  }));

});
