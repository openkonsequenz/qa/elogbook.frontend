/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { click } from '../../testing/index';
import { NgModule } from '@angular/core';
import { DebugElement } from '@angular/core';
import { MockComponent } from '../../testing/mock.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '../../testing/router-stubs';
import { MdDialogModule, MaterialModule, MdDialog } from '@angular/material';
import { SessionContext } from '../../common/session-context';
import { MainNavigationComponent } from './main-navigation.component';
import { MessageService } from 'app/services/message.service';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

@NgModule({
  declarations: [],
  entryComponents: [
    //LogoutComponent
  ]
})
class TestModule { }

describe('MainNavigationComponent', () => {
  let component: MainNavigationComponent;
  let fixture: ComponentFixture<MainNavigationComponent>;
  let routerStub: FakeRouter;
  let router: Router;
  let sessionContext: SessionContext;

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  class MockShiftChangeDialog extends AbstractMockObservableService {
    componentInstance = {
      isFetchingResp: false,
      isChangingShift: false};
    afterClosed() {
      return this;
    };
  }

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    router = new FakeRouter() as any as Router;
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        MaterialModule.forRoot()
      ],
      declarations: [
        MainNavigationComponent,
        MockComponent({ selector: 'input', inputs: ['options'] })
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: MdDialog, useClass: MdDialog },
        { provide: SessionContext, useValue: sessionContext },
        { provide: MessageService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavigationComponent);
    component = fixture.componentInstance;
  });

  it('should navigate to overview on home-button click', () => {
    component.goToOverview();    
    expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
  });

 it('should open the filter on click on the filter button', () => {    
    //WIP
    /*
    component.goToOverview();
    fixture.detectChanges(); // update view with array

    fixture.whenStable().then(() => { // wait for async getFinishedNotifications
      //fixture.detectChanges(); // update view with array
      const des = fixture.debugElement.queryAll(By.css('button'));
      click(des[0]);
    });
    */
  });

  it('should navigate to search on search-button click', () => {    
    component.goToSearchPage();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/search']);
  });

  it('should navigate to reminder on calendar-button click', () => {    
    component.goToReminderPage();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/reminders']);
  });

  it('should navigate to shiftChangeOverview on clock-button click', () => {    
    component.goToShiftChangeOverview();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/shiftChangeOverview']);
  });

  it('should navigate to mainview on logout', () => {    
    spyOn(sessionContext, 'clearStorage').and.callThrough();
    component.logout();
    expect(sessionContext.clearStorage).toHaveBeenCalled();    
  });

  it('should navigate to ShiftChange Dialog', async(() => {
    const mocker = new MockShiftChangeDialog();
    mocker.componentInstance.isChangingShift = false;
    mocker.componentInstance.isFetchingResp = false;
    
    spyOn(component.dialog, 'open').and.callFake(() => mocker );
    component.openDialogShiftChange( false );
    expect(component.dialog.open).toHaveBeenCalled();

    // play the other combinations
    mocker.componentInstance.isChangingShift = true;
    mocker.componentInstance.isFetchingResp = true;
    mocker.content = {};
    component.openDialogShiftChange( true );

    // play the other combinations
    mocker.componentInstance.isChangingShift = false;
    mocker.componentInstance.isFetchingResp = true;
    mocker.content = {};
    component.openDialogShiftChange( true );

    // play the other combinations
    mocker.componentInstance.isChangingShift = true;
    mocker.componentInstance.isFetchingResp = false;
    mocker.content = {};
    component.openDialogShiftChange( false );

  }));

});
