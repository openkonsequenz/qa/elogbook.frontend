/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, OnInit, Input } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { LogoutComponent } from '../../dialogs/logout/logout.component';
import { ShiftChangeComponent } from '../../dialogs/shift-change/shift-change.component';
import { MessageService } from '../../services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {

 
  private dialogConfig = new MdDialogConfig();
  private alertCounter = 0;

  constructor(
    public sessionContext: SessionContext,
    public dialog: MdDialog,
    public router: Router,
    private messageService: MessageService
    ) { }

  ngOnInit() {
    this.dialogConfig.disableClose = true;
  }

  openDialogLogout() {
    this.dialogConfig.data = this;
    const dialogRef = this.dialog.open(LogoutComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDialogShiftChange( fetchResponsibility: boolean ) {
    const dialogRef = this.dialog.open(ShiftChangeComponent, this.dialogConfig);
    dialogRef.componentInstance.isFetchingResp = fetchResponsibility;

    dialogRef.afterClosed().subscribe(() => this.onDialogShiftClosed( dialogRef.componentInstance.isFetchingResp, 
                                                                          dialogRef.componentInstance.isChangingShift ));
  }


  onDialogShiftClosed( isFetching: boolean, isChangingShift: boolean ) {
   if ( isChangingShift ) {
        if ( isFetching ) {
          this.sessionContext.setfilterMatrix(null);
          this.router.navigate(['logout']);
//          this.messageService.respChangedForCurrentUser$.emit(true);
        } else {
    this.openDialogLogout();
      }
    }
  }

  
  goToSearchPage() {
    this.router.navigate(['/search']);
  }

  setFilterExpansionState() {    
    this.sessionContext.setFilterExpansionState(!this.sessionContext.filterExpanded);
  }

  logout() {
    this.sessionContext.clearStorage();    
  }

  goToReminderPage() {
    this.router.navigate(['/reminders']);
  }

  goToShiftChangeOverview() {
    this.router.navigate(['/shiftChangeOverview']);
  }
  goToOverview() {
    this.router.navigate(['/overview']);
  }
}
