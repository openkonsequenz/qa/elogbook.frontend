/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { MockComponent } from './testing/mock.component';
import { AppComponent } from './app.component';
import { Http, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseDataLoaderService } from './services/jobs/base-data-loader.service';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { SessionContext } from './common/session-context';
import { MessageService } from './services/message.service';
import { ReminderCallerJobService } from './services/jobs/reminder-caller-job.service';
import { ImportFileCallerService } from './services/jobs/import-file-caller.service';
import { AbstractMockObservableService } from './common/abstract-mock-observable.service';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

class MockHttp extends AbstractMockObservableService {
  get() {
    return this;
  }
}

class MockHttpRes {
  public content: any;
  json() { return this.content; }
}

describe('AppComponent', () => {
  let router: Router;
  const mockService = new MockHttp();
  let sessionContext: SessionContext;

  beforeEach(() => {
    router = new FakeRouter() as any as Router;
    sessionContext = new SessionContext();    

    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockComponent({selector: 'app-main-navigation'}),
        MockComponent({selector: 'app-message-banner'}),
        MockComponent({selector: 'router-outlet'})
      ],
      providers: [
        { provide: Http, useValue: mockService },
        { provide: Router, useValue: router },
        { provide: BaseDataLoaderService, useValue: {} },
        { provide: ActivatedRoute },    
        { provide: HttpResponseInterceptorService },    
        { provide: SessionContext, useValue: sessionContext },
        { provide: MessageService },
        { provide: ReminderCallerJobService },
        { provide: ImportFileCallerService }
      ],      
    });
    TestBed.compileComponents();

    const httpRes = new MockHttpRes();
    httpRes.content = {};
    mockService.content = httpRes;
    

  });

  

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);    
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

 
  it('should extract the user data from the JWT-AccessToken', async(() => {
    const fixture = TestBed.createComponent(AppComponent);    
    const app = fixture.debugElement.componentInstance;

    const accessToken = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0e' +
      'VByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiI4ZmY5NTlhZC' +
      '02ODQ1LTRlOGEtYjRiYi02ODQ0YjAwMjU0ZjgiLCJleHAiOjE1MDY2MDA0NTAsIm5iZiI6MCwiaWF' +
      '0IjoxNTA2NjAwMTUwLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vs' +
      'b2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjM1OWVmOWM5LTc3ZGYtNGEzZ' +
      'C1hOWM5LWY5NmQ4MzdkMmQ1NyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbm' +
      'QiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI5NjVmNzM1MS0yZThiLTQ1MjgtOWYzZC1' +
      'lZTYyODNhOTViMTYiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNj' +
      'ZXNzIjp7InJvbGVzIjpbImVsb2dib29rLXN1cGVydXNlciIsImVsb2dib29rLW5vcm1hbHVzZXIiL' +
      'CJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7InJlYWxtLW1hbmFnZW1lbn' +
      'QiOnsicm9sZXMiOlsidmlldy11c2VycyIsInF1ZXJ5LWdyb3VwcyIsInF1ZXJ5LXVzZXJzIl19LCJ' +
      'hY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3Mi' +
      'LCJ2aWV3LXByb2ZpbGUiXX19LCJuYW1lIjoiQWRtaW5pc3RyYXRvciBBZG1pbmlzdHJhdG93aWNoI' +
      'iwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4iLCJnaXZlbl9uYW1lIjoiQWRtaW5pc3RyYXRvci' +
      'IsImZhbWlseV9uYW1lIjoiQWRtaW5pc3RyYXRvd2ljaCIsImVtYWlsIjoic2VyZ2VqLmtlcm5AcHRh' +
      'LmRlIiwicm9sZXN0ZXN0IjoiW2Vsb2dib29rLXN1cGVydXNlciwgZWxvZ2Jvb2stbm9ybWFsdXNlc' +
      'iwgdW1hX2F1dGhvcml6YXRpb24sIG9mZmxpbmVfYWNjZXNzLCB1bWFfYXV0aG9yaXphdGlvbiwgZW' +
      'xvZ2Jvb2stbm9ybWFsdXNlcl0ifQ.o94Bl43oqyLNzZRABvIq9z-XI8JQjqj2FSDdUUEZGZPTN4uw' +
      'D5fyi0sONbDxmTFvgWPh_8ZhX6tlDGiupVDBY4eRH43Eettm-t4CDauL7FzB3w3dDPFMB5DhP4rrp' +
      'k_kATwnY2NKLRbequnh8Z6wLXjcmQNLgrgknXB_gogWAqH29dqKexwceMNIbq-kjaeLsmHSXM9TE9' +
      'q7_Ln9el04OlkpOVspVguedfINcNFg0DmYLJWyD2ORkOHLmYigN6YnyB9P2NFOnKGlLuQ87GjosI0' +
      '0zBniRGi3PhE9NGd51Qggdbcsm0aM8GiMaZ7SO5i8iQWL10TRFRFyTEfy6hSO8g';

      const incognito: any = app;
      incognito.processAccessToken( accessToken );

      expect( sessionContext.getCurrUser().name).toBe('Administrator Administratowich');
      expect( sessionContext.getAccessToken()).toBe( accessToken );
  }));

  
  it('read the param access token', () => {
    const fixture = TestBed.createComponent(AppComponent);    
    const app = fixture.debugElement.componentInstance;
    const token = 'AJDHDND';
    const uri = 'a=X&b=y&accessToken=' + token;
    const inkognito: any = app;
    const token2 = inkognito.readParamAccessToken(uri);

    expect( token2 ).toBe(token);
  });
  
});
