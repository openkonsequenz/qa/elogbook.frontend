/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { BaseHttpService } from './base-http.service';
import { User } from '../model/user';

import { MessageService } from './message.service';
import { UserSettings } from 'app/model/user-settings';

@Injectable()
export class UserService extends BaseHttpService  {

    constructor(
        private _http: Http,
        public messageService: MessageService,
        private _sessionContext: SessionContext) {
        super(messageService);        
    }

    public getUsers(): Observable<User[]> {
        const headers = new Headers();
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(super.getBaseUrl() + '/users', { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
             .catch(error => {        
                  return super.handleErrorPromise(error);      
                 });
    }

    public getUserSettings(): Observable<UserSettings> {
        const headers = new Headers();
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(super.getBaseUrl() + '/user-settings', { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
             .catch(error => {        
                  return super.handleErrorPromise(error);      
                 });
    }

    public postUserSettings(userSettings: UserSettings) {
        const headers = new Headers();
        const url = super.getBaseUrl() + '/user-settings/';
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.post(url, JSON.stringify(userSettings, this.replacer), { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }

    replacer(key, value) {
        const originalObject = this[key];
        if(originalObject instanceof Map) {
          return {
            dataType: 'Map',
            value: Array.from(originalObject.entries()), // or with spread: value: [...originalObject]
          };
        } else {
          return value;
        }
    }

    reviver(key, value) {
        if(typeof value === 'object' && value !== null) {
          if (value.dataType === 'Map') {
            return new Map(value.value);
          }
        }
        return value;
    }
    
}
