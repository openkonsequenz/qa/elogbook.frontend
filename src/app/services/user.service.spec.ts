/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { UserService } from './user.service';
import { User } from '../model/user';
import { USERS } from '../test-data/users';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { MessageService } from '../services/message.service';

describe('Http-UserService (mockBackend)', () => {
  let sessionContext: SessionContext;
  let messageService: MessageService;
  beforeEach( async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        UserService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext,
        MessageService
      ]
    })
    .compileComponents();
    messageService = new MessageService();
    sessionContext = new SessionContext();
  }));

  it('can instantiate service when inject service',
    inject([UserService], (service: UserService) => {
      expect(service instanceof UserService).toBe(true);
  }));



  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new UserService(http, messageService, sessionContext );
    expect(service instanceof UserService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
  }));

  describe('when getResponsibilities()', () => {
    let backend: MockBackend;
    let service: UserService;
    let response: Response;
    const fakeResps: User[] = USERS;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new UserService(http, messageService, sessionContext);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({status: 200, body: fakeResps, headers: successHeaders} );
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));
    it('should have expected fake users (then)', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getUsers().toPromise()
          .then( users => {
            expect( users.length ).toBe(3);
            expect( users[0] ).not.toBeNull();
            expect( users[1] ).not.toBeNull();
            expect( users[2] ).not.toBeNull();
            expect( users[0].id ).toBe('1');
            expect( users[1].id ).toBe('2');
            expect( users[2].id ).toBe('3');
            expect( sessionContext.getCurrSessionId() ).not.toBe('SuperVALID!'); // only done with authentification
          });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
        const resp = new Response(new ResponseOptions({status: 404}));
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

        service.getUsers()
          .do(responsibilities => {
            fail('should not respond with users');
          })
          .catch(err => {
            const str = err;
            expect(err).toMatch('Bad response status', 'should catch bad response status code');
            return Observable.of(null); // failure is the expected test result
          })
          .toPromise();
      })));
  });  
});
