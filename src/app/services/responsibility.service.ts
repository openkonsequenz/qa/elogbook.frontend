/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { BaseHttpService } from './base-http.service';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { NotificationSearchFilter } from '../model/notification-search-filter';
import { ResponsibilitySearchFilter } from '../model/responsibility-search-filter';
import { HistoricalShiftChanges } from '../model/historical-shift-changes';
import { HistoricalResponsibility } from '../model/historical-responsibility';
import { MessageService } from './message.service';

@Injectable()
export class ResponsibilityService extends BaseHttpService {

    constructor(
        private _http: Http,
        private _sessionContext: SessionContext,
        public messageService: MessageService) {
        super(messageService);
    }

    public getAllResponsibilities(): Observable<TerritoryResponsibility[]> {
        const headers = new Headers();
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(super.getBaseUrl() + '/allResponsibilities', { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => {
                return super.handleErrorPromise(error);
            });
    }

    public getResponsibilities(): Observable<TerritoryResponsibility[]> {
        const headers = new Headers();
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(super.getBaseUrl() + '/currentResponsibilities', { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }

    public getPlannedResponsibilities(): Observable<TerritoryResponsibility[]> {
        const headers = new Headers();
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(super.getBaseUrl() + '/plannedResponsibilities', { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }

    public planResponsibilities(responsibilities: TerritoryResponsibility[]) {
        const headers = new Headers();
        const url = super.getBaseUrl() + '/postResponsibilities/';
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.post(url, JSON.stringify(responsibilities), { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }

    public fetchResponsibilities(responsibilities: TerritoryResponsibility[]) {
        const headers = new Headers();
        const url = super.getBaseUrl() + '/fetchResponsibilities/';
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.post(url, JSON.stringify(responsibilities), { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }
    
    public confirmResponsibilities(responsibilities: TerritoryResponsibility[]) {
        const headers = new Headers();
        const url = super.getBaseUrl() + '/confirmResponsibilities/';
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.post(url, JSON.stringify(responsibilities), { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }

    public getHistoricalShiftChangeList(notificationSearchFilter: ResponsibilitySearchFilter): Observable<HistoricalShiftChanges> {
        const headers = new Headers();
        const url = super.getBaseUrl() + '/shiftChangeList/';
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.post(url, JSON.stringify(notificationSearchFilter), { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }

    public getHistoricalResponsibilities(transactionId: number): Observable<TerritoryResponsibility[]> {
        const headers = new Headers();
        const url = super.getBaseUrl() + '/historicalResponsibilities/' + transactionId;
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(url, { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(error => { return super.handleErrorPromise(error); });
    }
}
