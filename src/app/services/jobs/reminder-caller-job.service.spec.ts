/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, fakeAsync, tick, TestBed, ComponentFixture, inject, discardPeriodicTasks } from '@angular/core/testing';
import { MessageService, MessageDefines } from '../../services/message.service';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { ReminderCallerJobService } from './reminder-caller-job.service';
import { REMINDER_NOTIFICATIONS } from '../../test-data/reminder-notifications';
import { Globals } from '../../common/globals';


describe('ReminderCallerJobService', () => {
  class MockReminderService extends AbstractMockObservableService {

    getCurrentReminders() {
      return this;
    }
  }

  let sessionContext: SessionContext;
  let messageService: MessageService;
  let mockReminderService;
  let injectedService;


  beforeEach(async(() => {
    mockReminderService = new MockReminderService();
    sessionContext = new SessionContext();
    sessionContext.clearStorage();
    messageService = new MessageService();

    injectedService = new ReminderCallerJobService(sessionContext, mockReminderService, messageService);

  }));

  it('should init the timer after successfully login', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();  
    expect(injectedService.init).toHaveBeenCalledTimes(0);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick();

    expect(injectedService.init).toHaveBeenCalledTimes(1);    
    discardPeriodicTasks();    
  }));

  it('should destroy the timer after logout', fakeAsync(() => {
    spyOn(injectedService, 'destroy').and.callThrough();   
    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick();

    expect(injectedService.destroy).toHaveBeenCalledTimes(0);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);
    tick();

    expect(injectedService.destroy).toHaveBeenCalledTimes(1);    
    discardPeriodicTasks();    
  }));

  it('should set reminderAvailable to true when remindernotifications exist', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();
    mockReminderService.content = REMINDER_NOTIFICATIONS;    
    Globals.REMINDER_JOB_POLLING_START_DELAY = 10;
    expect(sessionContext.reminderAvailable).toBe(false);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);    

    expect(injectedService.init).toHaveBeenCalledTimes(1);
    expect(sessionContext.reminderAvailable).toBe(true);
    discardPeriodicTasks();    
  }));

  it('should set reminderAvailable to false when the are no remindernotifications', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();    
    mockReminderService.content = [];    
    Globals.REMINDER_JOB_POLLING_START_DELAY = 10;
    expect(sessionContext.reminderAvailable).toBe(false);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);    

    expect(injectedService.init).toHaveBeenCalledTimes(1);
    expect(sessionContext.reminderAvailable).toBe(false);
    discardPeriodicTasks();    
  }));

  it('should call "setError" when an error occurs while running "doJob"', fakeAsync(() => {
    spyOn(injectedService, 'setError').and.callThrough();    
    Globals.REMINDER_JOB_POLLING_START_DELAY = 10;
    mockReminderService.error = 'REMINDER_MOCK_ERROR';
    expect(sessionContext.reminderAvailable).toBe(false);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);    

    expect(injectedService.setError).toHaveBeenCalledTimes(1);    
    discardPeriodicTasks();    
  }));


});