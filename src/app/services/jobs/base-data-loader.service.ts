/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { MessageService, MessageDefines } from '../message.service';
import { BaseDataService } from '../base-data.service';
import { UserService } from '../user.service';
import { SessionContext } from '../../common/session-context';


@Injectable()
export class BaseDataLoaderService {

  constructor(
    private baseDataService: BaseDataService,
    private userService: UserService,
    private msgService: MessageService,
    private sessionContext: SessionContext
  ) {
    this.msgService.loginLogoff$.subscribe(msg => this.onLoginLogoff(msg));
  }

  onLoginLogoff(msg: string): void {
    this.sessionContext.initBannerMessage();
    if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
      this.loadBaseData();
    }
  }

  loadBaseData() {
    this.baseDataService.getBranches()
      .subscribe(res => this.sessionContext.setBranches(res),
      error => {
        console.log(error);
      });

    this.baseDataService.getStatuses()
      .subscribe(res => this.sessionContext.setStatuses(res),
      error => {
        console.log(error);
      });

    this.baseDataService.getPriorities()
      .subscribe(res => this.sessionContext.setPriorities(res),
      error => {
        console.log(error);
      });

    this.baseDataService.getGridTerritories()
      .subscribe(res => this.sessionContext.setGridTerritories(res),
      error => {
        console.log(error);
      });


    this.userService.getUsers()
      .subscribe(res => this.sessionContext.setAllUsers(res),
      error => {
        console.log(error);
      });
    
    this.userService.getUserSettings()
    .subscribe(res => {      
      this.sessionContext.setUsersSettings(res)
    } , 
    error => {
      console.log(error);
    });
  }

}
