/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Subscription, Observable } from 'rxjs/Rx';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { ReminderSearchFilter } from '../../model/reminder-search-filter';
import { ImportService } from '../../services/import.service';
import { SessionContext } from '../../common/session-context';
import { MessageService, MessageDefines } from '../../services/message.service';
import { Globals } from '../../common/globals';
import { NotificationFileModel } from '../../model/file-model';


@Injectable()
export class ImportFileCallerService {

    private timer;
    private subscription: Subscription;
    currentTime$: string;


    constructor(
        protected _sessionContext: SessionContext,
        protected importFileService: ImportService,
        protected msgService: MessageService
    ) {
        if (Globals.IMPORT_JOB_POLLING_ON) {
            this.msgService.loginLogoff$.subscribe(msg => this.onLoginLogoff(msg));
        }
    }

    onLoginLogoff(msg: string) {
        if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
            this.init();
        }
        if (msg === MessageDefines.MSG_LOG_OFF) {
            this.destroy();
        }
    }

    init() {
        this.timer = TimerObservable.create(Globals.IMPORT_JOB_POLLING_START_DELAY, Globals.IMPORT_JOB_POLLING_INTERVALL);
        this.subscription = this.timer.subscribe(t => this.doJob(t));
    }

    destroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    doJob(tick: number) {
        this.currentTime$ = new Date().toISOString();

        this.importFileService.getImportFiles().subscribe(files => this.setImportFileAvailableStatus(files),
            error => {
                console.log(error);
                this.setError(error);
            });

        this.importFileService.importFile().subscribe(files => this.setImportFileAvailableStatus(files),
            error => {
                console.log(error);
                this.setError(error);
            });
    }

    protected setError(showErr: boolean) {
    }



    setImportFileAvailableStatus(files: NotificationFileModel[]) {
        if (files && files.length) {
            // not empty; file available                         
            this._sessionContext.setImportFileAvailable(true);
        } else {
            // empty; no file available            
            this._sessionContext.setImportFileAvailable(false);
        }
    }


}
