/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Subscription, Observable } from 'rxjs/Rx';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { ReminderSearchFilter } from '../../model/reminder-search-filter';
import { ReminderService } from '../../services/reminder.service';
import { SessionContext } from '../../common/session-context';
import { Notification } from '../../model/notification';
import { MessageService, MessageDefines } from '../../services/message.service';
import { Globals } from '../../common/globals';
import { FilterMatrix } from '../../model/controller-model/filter-matrix';


@Injectable()
export class ReminderCallerJobService {

  private timer;
  private subscription: Subscription;
  currentTime$: string;
  reminderSearchFilter: ReminderSearchFilter;


  constructor(
    protected _sessionContext: SessionContext,
    protected reminderService: ReminderService,
    protected msgService: MessageService
  ) {
    if (Globals.REMINDER_JOB_POLLING_ON) {
      this.msgService.loginLogoff$.subscribe(msg => this.onLoginLogoff(msg));
    }
  }

  onLoginLogoff(msg: string) {
    if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
      this.init();
    }
    if (msg === MessageDefines.MSG_LOG_OFF) {
      this.destroy();
    }
  }

  init() {
    this.reminderSearchFilter = new ReminderSearchFilter();
    this.timer = TimerObservable.create(Globals.REMINDER_JOB_POLLING_START_DELAY, Globals.REMINDER_JOB_POLLING_INTERVALL);
    this.subscription = this.timer.subscribe(t => this.doJob(t));
  }

  destroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  doJob(tick: number) {
    this.currentTime$ = new Date().toISOString();
    this.reminderSearchFilter.reminderDate = this.currentTime$;
    const filterMatrixSession = this._sessionContext.getfilterMatrix();
    if (filterMatrixSession) {
      const filterMatrix: FilterMatrix = new FilterMatrix(this._sessionContext.getfilterMatrix().responsibilityContainerMatrix);
      this.reminderSearchFilter.responsibilityFilterList = filterMatrix.getNumFilterList();
    }

    this.reminderService.getCurrentReminders(this.reminderSearchFilter)
      .subscribe(nots => this.setReminderAvailableStatus(nots),
        error => {
          console.log(error);
          this.setError(error);
        });
  }

  protected setError(showErr: boolean) {
  }

  setReminderAvailableStatus(nots: Notification[]) {
    if (nots && nots.length) {
      // not empty; reminders available                         
      this._sessionContext.setReminderAvailable(true);
    } else {
      // empty; no reminders available            
      this._sessionContext.setReminderAvailable(false);
    }
  }


}
