/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, fakeAsync, tick, TestBed, ComponentFixture, inject, discardPeriodicTasks } from '@angular/core/testing';
import { MessageService, MessageDefines } from '../../services/message.service';
import { Observable } from 'rxjs/Observable';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { ImportFileCallerService } from './import-file-caller.service';
import { IMPORT_NOTIFICATION_FILES } from '../../test-data/import-notification-files';
import { Globals } from '../../common/globals';


describe('ImportFileCallerService', () => {
  class MockImportService extends AbstractMockObservableService {

    getImportFiles() {
      return Observable.of(IMPORT_NOTIFICATION_FILES);
    }

    deleteFile(fileName: string) {
      return Observable.of(false);
    }

    importFile() {
      return Observable.of(IMPORT_NOTIFICATION_FILES);
    }
  }

  let sessionContext: SessionContext;
  let messageService: MessageService;
  let mockImportService;
  let injectedService;


  beforeEach(async(() => {
    mockImportService = new MockImportService();
    sessionContext = new SessionContext();
    sessionContext.clearStorage();
    messageService = new MessageService();

    injectedService = new ImportFileCallerService(sessionContext, mockImportService, messageService);

  }));

  it('should init the timer after successfully login', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();
    expect(injectedService.init).toHaveBeenCalledTimes(0);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick();

    expect(injectedService.init).toHaveBeenCalledTimes(1);
    discardPeriodicTasks();
  }));

  it('should destroy the timer after logout', fakeAsync(() => {
    spyOn(injectedService, 'destroy').and.callThrough();
    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick();

    expect(injectedService.destroy).toHaveBeenCalledTimes(0);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);
    tick();

    expect(injectedService.destroy).toHaveBeenCalledTimes(1);
    discardPeriodicTasks();
  }));

  it('should set importFileAvailable to true when importfiles exist', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();
    mockImportService.content = IMPORT_NOTIFICATION_FILES;
    Globals.IMPORT_JOB_POLLING_START_DELAY = 10;
    expect(sessionContext.importFileAvailable).toBe(false);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);

    expect(injectedService.init).toHaveBeenCalledTimes(1);
    expect(sessionContext.importFileAvailable).toBe(true);
    discardPeriodicTasks();
  }));



  it('should set importFileAvailable to false when the are no importfiles', fakeAsync(() => {

    mockImportService.content = [];
    Globals.IMPORT_JOB_POLLING_START_DELAY = 10;

    expect(sessionContext.importFileAvailable).toBe(false);

    discardPeriodicTasks();
  }));

  /*
  it('should call "setError" when an error occurs while running "doJob"', fakeAsync(() => {
    spyOn(injectedService, 'setError').and.callThrough();
    Globals.REMINDER_JOB_POLLING_START_DELAY = 10;
    mockImportService.error = 'IMPORT_MOCK_ERROR';
    expect(sessionContext.importFileAvailable).toBe(false);

    messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);

    expect(injectedService.setError).toHaveBeenCalledTimes(1);
    discardPeriodicTasks();
  }));
*/

});