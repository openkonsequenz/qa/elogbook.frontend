/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, fakeAsync, tick, TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { Status } from '../../model/status';
import { Branch } from '../../model/branch';
import { GridTerritory } from '../../model/gridterritory';
import { User } from '../../model/user';
import { MessageService, MessageDefines } from '../../services/message.service';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { BaseDataService } from '../base-data.service';
import { USERS} from '../../test-data/users';
import { BaseDataLoaderService } from './base-data-loader.service';
import { Priority } from 'app/model/priority';

describe('BaseDataLoaderService', () => {
  class MockDataService extends AbstractMockObservableService {
    public statusList: Status[];
    public branchList: Branch[];
    public prioritiesList: Priority[];
    public gridTerritoryList: GridTerritory[];
    public statusError;
    public branchError;
    public gridTerritoryError;
    public priorityError;

    getStatuses() {
      const newService = new MockDataService();
      newService.content = this.statusList;
      newService.error = this.statusError;
      return newService;
    }
    getBranches() {
      const newService = new MockDataService();
      newService.content = this.branchList;
      newService.error = this.branchError;
      return newService;
    }
    getGridTerritories() {
      const newService = new MockDataService();
      newService.content = this.gridTerritoryList;
      newService.error = this.gridTerritoryError;
      return newService;
    }
    
    getPriorities() {    
      const newService = new MockDataService();
      newService.content = this.prioritiesList;
      newService.error = this.priorityError;            
      return newService;
    }
  }

  class MockUserService extends AbstractMockObservableService {
    public userList: User[];
    public userError;

    getUsers() {
      const newService = new MockDataService();
      newService.content = this.userList;
      newService.error = this.userError;
      return newService;
    }

    getUserSettings() {
      return this;
    };

    postUserSettings() {
      return this;
    }; 

  }
  let sessionContext: SessionContext;
  let messageService: MessageService;
  let mockDataService;  
  let mockUserService;


  beforeEach(async(() => {
    mockDataService = new MockDataService(); 
    mockUserService = new MockUserService();
    sessionContext = new SessionContext();
    sessionContext.clearStorage();
    messageService = new MessageService();

    const loaderServer = new BaseDataLoaderService( mockDataService, mockUserService, messageService, sessionContext );

  }));

  it('should be load the base-data after MSG_LOG_IN_SUCCEEDED-Message "', fakeAsync(() => {
    mockDataService.statusList = [{ id: 1, name: 'offen' }];
    mockDataService.branchList = [{ id: 1, name: 'W', description: 'Wasser' }];
    mockDataService.gridTerritoryList = [{ id: 1, name: 'MA', description: 'Mannheim', fkRefMaster: 1 }];
    mockDataService.prioritiesList = [{ id: 1, name: 'Wichtig', weighting: 30, imageName: "prio-important-icon-web.svg" }];
    mockUserService.userList = USERS;

    expect(sessionContext.getStatuses()).toBeNull();
    expect(sessionContext.getBranches()).toBeNull();
    expect(sessionContext.getGridTerritories()).toBeNull();
    expect(sessionContext.getAllUsers()).toBeNull();

    messageService.loginLogoff$.emit( MessageDefines.MSG_LOG_IN_SUCCEEDED );

    tick();

    expect(sessionContext.getStatuses().length).toBe(1);
    expect(sessionContext.getStatuses()[0].name).toBe('offen');
    
    expect(sessionContext.getBranches().length).toBe(1);    
    expect(sessionContext.getBranches()[0].description).toBe('Wasser');

    expect(sessionContext.getGridTerritories().length).toBe(1);
    expect(sessionContext.getGridTerritories()[0].name).toBe('MA');

    expect(sessionContext.getAllUsers().length).toBe(3);
  }));
});
