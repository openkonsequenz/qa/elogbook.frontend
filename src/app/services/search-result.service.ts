/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { SessionContext } from '../common/session-context';
import { BaseHttpService } from './base-http.service';
import { Notification } from '../model/notification';
import { GlobalSearchFilter } from '../model/global-search-filter';
import { MessageService } from './message.service';

@Injectable()
export class SearchResultService extends BaseHttpService {

  constructor(private _http: Http,
    private _sessionContext: SessionContext,
    public messageService: MessageService) {
    super(messageService);
  }

  public getSearchResults(searchResultFilter?: GlobalSearchFilter): Observable<Notification[]> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/searchResults/';
    const globalSearchFilter = searchResultFilter || {};
    this.createCommonHeaders(headers, this._sessionContext);
    return this._http.post(url, JSON.stringify(globalSearchFilter), { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch(error => { return super.handleErrorPromise(error); });
  }

}
