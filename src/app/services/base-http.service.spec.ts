/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BaseHttpService } from './base-http.service';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { MessageService } from '../services/message.service';
describe('BaseHttpService', () => {
  const _sc: SessionContext = new SessionContext();
  class BaseHttpServiceMock extends BaseHttpService {
    public constructor( public messageService: MessageService ) {
      super(messageService );
    }
    public getBaseUrl(): string {
      return super.getBaseUrl();
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BaseHttpService]
    });

    it( 'should deliver the BaseUrl', () => {
      const bhs = new BaseHttpServiceMock(this.messageService);
      expect( bhs.getBaseUrl()).toBe(Globals.BASE_URL);
    });
  });


});
