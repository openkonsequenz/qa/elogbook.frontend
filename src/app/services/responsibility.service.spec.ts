/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { ResponsibilityService } from './responsibility.service';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { RESPONSIBILITIES } from '../test-data/responsibilities';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { MessageService } from '../services/message.service';

describe('Http-ResponsibilityService (mockBackend)', () => {
  let sessionContext: SessionContext;
  let messageService: MessageService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        ResponsibilityService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext,
        MessageService
      ]
    })
      .compileComponents();
    sessionContext = new SessionContext();
    messageService = new MessageService();
  }));

  it('can instantiate service when inject service',
    inject([ResponsibilityService], (service: ResponsibilityService) => {
      expect(service instanceof ResponsibilityService).toBe(true);
    }));



  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new ResponsibilityService(http, sessionContext, messageService);
    expect(service instanceof ResponsibilityService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
    }));


  describe('when getPlannedResponsibilities()', () => {
    let backend: MockBackend;
    let service: ResponsibilityService;
    let response: Response;
    const fakeResps: TerritoryResponsibility[] = RESPONSIBILITIES;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new ResponsibilityService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({ status: 200, body: fakeResps, headers: successHeaders });
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));
    it('should have expected fake responsibilities (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getPlannedResponsibilities().toPromise()
        .then(responsibilities => {
          expect(responsibilities.length).toBe(3);
          expect(responsibilities[0]).not.toBeNull();
          expect(responsibilities[1]).not.toBeNull();
          expect(responsibilities[2]).not.toBeNull();
          expect(responsibilities[0].id).toBe(1);
          expect(responsibilities[1].id).toBe(2);
          expect(responsibilities[2].id).toBe(3);
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));
    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.getPlannedResponsibilities()
        .do(responsibilities => {
          fail('should not respond with responsibilities');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));
  });
  describe('when getResponsibilities()', () => {
    let backend: MockBackend;
    let service: ResponsibilityService;
    let response: Response;
    const fakeResps: TerritoryResponsibility[] = RESPONSIBILITIES;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new ResponsibilityService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({ status: 200, body: fakeResps, headers: successHeaders });
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));
    it('should have expected fake responsibilities (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getResponsibilities().toPromise()
        .then(responsibilities => {
          expect(responsibilities.length).toBe(3);
          expect(responsibilities[0]).not.toBeNull();
          expect(responsibilities[1]).not.toBeNull();
          expect(responsibilities[2]).not.toBeNull();
          expect(responsibilities[0].id).toBe(1);
          expect(responsibilities[1].id).toBe(2);
          expect(responsibilities[2].id).toBe(3);
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));
  });
  describe('when getAllResponsibilities()', () => {
    let backend: MockBackend;
    let service: ResponsibilityService;
    let response: Response;
    const fakeResps: TerritoryResponsibility[] = RESPONSIBILITIES;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new ResponsibilityService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({ status: 200, body: fakeResps, headers: successHeaders });
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));
    it('should have expected fake responsibilities (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getAllResponsibilities().toPromise()
        .then(responsibilities => {
          expect(responsibilities.length).toBe(3);
          expect(responsibilities[0]).not.toBeNull();
          expect(responsibilities[1]).not.toBeNull();
          expect(responsibilities[2]).not.toBeNull();
          expect(responsibilities[0].id).toBe(1);
          expect(responsibilities[1].id).toBe(2);
          expect(responsibilities[2].id).toBe(3);
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));
  });

  describe('when confirmResponsibilities()', () => {
    let backend: MockBackend;
    let service: ResponsibilityService;
    let response: Response;
    const fakeResps = { 'ret': 'OK' };


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new ResponsibilityService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({ status: 200, body: fakeResps, headers: successHeaders });
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));
    it('should have expected fake response({"ret":"OK"})', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.confirmResponsibilities(RESPONSIBILITIES).toPromise()
        .then(resp => {
          expect(resp['ret']).toBe('OK');
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));
    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.confirmResponsibilities(RESPONSIBILITIES)
        .do(responsibilities => {
          fail('should not respond with responsibilities');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));
  });

  describe('when planResponsibilities()', () => {
    let backend: MockBackend;
    let service: ResponsibilityService;
    let response: Response;
    const fakeResps: TerritoryResponsibility[] = RESPONSIBILITIES;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new ResponsibilityService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({ status: 200, body: { ret: 'OK' }, headers: successHeaders });
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));
    it('should have expected fake responsibilities (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.planResponsibilities(fakeResps).toPromise()
        .then(returnObj => {
          expect(returnObj.ret).toBe('OK');
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.planResponsibilities(fakeResps)
        .do(ret => {
          fail('should not respond with responsibilities');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));
  });
});

