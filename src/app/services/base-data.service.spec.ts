/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { BaseDataService } from '../services/base-data.service';
import { Branch } from '../model/branch';
import { GridTerritory } from '../model/gridterritory';
import { Status } from '../model/status';
import { STATUSES } from '../test-data/statuses';
import { BRANCHES } from '../test-data/branches';
import { GRIDTERRITORIES } from '../test-data/gridterritories';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { MessageService } from '../services/message.service';

describe('Http-BaseDataService (mockBackend)', () => {
  let sessionContext: SessionContext;
  let messageService: MessageService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        BaseDataService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext,
        MessageService
      ]
    })
      .compileComponents();
    sessionContext = new SessionContext();
    messageService = new MessageService();
  }));

  it('can instantiate service when inject service',
    inject([BaseDataService], (service: BaseDataService) => {
      expect(service instanceof BaseDataService).toBe(true);
    }));

  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new BaseDataService(http, messageService, sessionContext);
    expect(service instanceof BaseDataService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
    }));

  describe('when getBranches()', () => {
    let backend: MockBackend;
    let service: BaseDataService;
    let fakeBranches: Branch[];

    let response: Response;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new BaseDataService(http, messageService, sessionContext);
      fakeBranches = BRANCHES;
      const options = new ResponseOptions({ status: 200, body: fakeBranches });
      response = new Response(options);
    }));
    it('should have expected fake branches (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getBranches().toPromise()
        .then(branches => {
          expect(branches.length).toBe(5);
        });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.getBranches()
        .do(versionInfo => {
          fail('should not respond with versionInfo');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));
  });

  describe('when getGridTerritories()', () => {
    let backend: MockBackend;
    let service: BaseDataService;
    let fakeGridTerritories: GridTerritory[];

    let response: Response;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new BaseDataService(http, messageService, sessionContext);
      fakeGridTerritories = GRIDTERRITORIES;
      const options = new ResponseOptions({ status: 200, body: fakeGridTerritories });
      response = new Response(options);
    }));
    it('should have expected fake grid-territories (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getGridTerritories().toPromise()
        .then(gridTerritories => {
          expect(gridTerritories.length).toBe(4);
        });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.getGridTerritories()
        .do(versionInfo => {
          fail('should not respond with versionInfo');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));
  });

  describe('when getNotificationStatuses()', () => {
    let backend: MockBackend;
    let service: BaseDataService;
    let fakeStatuses: Status[];

    let response: Response;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new BaseDataService(http, messageService, sessionContext);
      fakeStatuses = STATUSES;
      const options = new ResponseOptions({ status: 200, body: fakeStatuses });
      response = new Response(options);
    }));
    it('should have expected fake notification statuses (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getStatuses().toPromise()
        .then(statuses => {
          expect(statuses.length).toBe(4);
        });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.getStatuses()
        .do(versionInfo => {
          fail('should not respond with versionInfo');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));
  });

});
