/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { AuthenticationService } from './authentication.service';
import { LoginCredentials } from '../model/login-credentials';
import { User } from '../model/user';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { MessageService } from '../services/message.service';

describe('Http-AuthenticationService (mockBackend)', () => {
  let sessionContext: SessionContext;
  let messageService: MessageService;
  beforeEach( async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        AuthenticationService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext,
        MessageService
      ]
    })
    .compileComponents();
    sessionContext = new SessionContext();
    messageService = new MessageService();
  }));

  it('can instantiate service when inject service',
    inject([AuthenticationService], (service: AuthenticationService) => {
      expect(service instanceof AuthenticationService).toBe(true);
  }));



  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new AuthenticationService(http, messageService, sessionContext );
    expect(service instanceof AuthenticationService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
  }));

  describe('when login()', () => {
    let backend: MockBackend;
    let service: AuthenticationService;
    let response: Response;
    const fakeCreds: LoginCredentials = { userName: 'carlo', password: 'newPwd'};
    const fakeUser: User = { id: '44',  username: 'carlo', password: 'serverPwd'
                         , name: 'Carlo Cottura', specialUser: true, itemName: this.name };


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new AuthenticationService(http, messageService, sessionContext);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({status: 200, body: fakeUser, headers: successHeaders} );
      response = new Response(options);
    }));
 
  });
 });
