/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { NotificationSearchFilter } from '../model/notification-search-filter';
import { MessageService } from './message.service';
import { SessionContext } from '../common/session-context';
import { BaseHttpService } from './base-http.service';
import { Notification } from '../model/notification';
import { FilterSelection } from '../model/filter-selection';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { ContactTupel } from 'app/model/contact-tupel';

@Injectable()
export class NotificationService extends BaseHttpService {
  public static ALL = 'active';
  public static CURRENT = 'current';
  public static FUTURE = 'future';
  public static OPEN = 'open';
  public static FINISHED = 'past';

  public itemAdded$: EventEmitter<Notification>;
  public itemChanged$: EventEmitter<Notification>;

  constructor(
    private _http: Http,
    private _sessionContext: SessionContext,
    public messageService: MessageService,
  ) {
    super(messageService);
    this.itemAdded$ = new EventEmitter();
    this.itemChanged$ = new EventEmitter();

  }

  public getNotifications(notificationType: string, notificationSearchFilter?: NotificationSearchFilter): Observable<Notification[]> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/notifications/' + notificationType;
    const filter = notificationSearchFilter || {};
    this.createCommonHeaders(headers, this._sessionContext);
    return this._http.post(url, JSON.stringify(filter), { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch(error => {
        return super.handleErrorPromise(error);
      });
  }

  public getNotification(notificationId: number): Observable<Notification> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/notification/' + notificationId;
    this.createCommonHeaders(headers, this._sessionContext);
    return this._http.get(url, { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch((error) => {
        return super.handleErrorPromise(error);
      });
  }

  public getAssignedUserSuggestions(): Observable<ContactTupel[]> {
    const headers = new Headers();
    this.createCommonHeaders(headers, this._sessionContext);
    return this._http.get(super.getBaseUrl() + '/assignedUserSuggestions/', { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch((error) => {
        return super.handleErrorPromise(error);
      });
  }

  public getNotificationVersions(incidentId: number): Observable<Notification[]> {
    const headers = new Headers();
    this.createCommonHeaders(headers, this._sessionContext);
    return this._http.get(super.getBaseUrl() + '/notificationsByIncident/' + incidentId, { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch((error) => {
        return super.handleErrorPromise(error);
      });
  }

  public createNotification(notification: Notification): Observable<Notification> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/notifications/create/';

    this.createCommonHeaders(headers, this._sessionContext);

    return this._http.put(url, JSON.stringify(notification), { headers: headers })
      .map(res => {
        const newNotification = super.extractData(res, this._sessionContext);
        this.itemAdded$.emit(newNotification);
        return newNotification;
      })
      .catch((error) => { 
        return super.handleErrorPromise(error); 
      });
  }

  public updateNotification(notification: Notification): Observable<Notification> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/notifications/update/';

    this.createCommonHeaders(headers, this._sessionContext);

    return this._http.post(url, JSON.stringify(notification), { headers: headers })
      .map(res => {
        const changedNotification = super.extractData(res, this._sessionContext);
        this.itemChanged$.emit(changedNotification);
        return changedNotification;
      })
      .catch((error) => {
        return super.handleErrorPromise(error);
      });
  }

  public getAllNotifications(): Observable<Notification[]> {
    return this.getNotifications(NotificationService.ALL);
  }
  public getCurrentNotifications(): Observable<Notification[]> {
    return this.getNotifications(NotificationService.CURRENT);
  }
  public getFutureNotifications(notificationSearchFilter: NotificationSearchFilter): Observable<Notification[]> {
    return this.getNotifications(NotificationService.FUTURE, notificationSearchFilter);
  }
  public getOpenNotifications(notificationSearchFilter: NotificationSearchFilter): Observable<Notification[]> {
    return this.getNotifications(NotificationService.OPEN, notificationSearchFilter);
  }
  public getFinishedNotifications(notificationSearchFilter: NotificationSearchFilter): Observable<Notification[]> {
    return this.getNotifications(NotificationService.FINISHED, notificationSearchFilter);
  }

}
