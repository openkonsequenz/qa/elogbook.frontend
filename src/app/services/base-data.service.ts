/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable } from '@angular/core';
import { Http, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { BaseHttpService } from './base-http.service';
import { Branch } from '../model/branch';
import { Status } from '../model/status';
import { GridTerritory } from '../model/gridterritory';
import { GRIDTERRITORIES } from '../test-data/gridterritories';
import { MessageService } from './message.service';
import { Priority } from 'app/model/priority';

@Injectable()
export class BaseDataService extends BaseHttpService {

  constructor(
    private _http: Http,
    public messageService: MessageService,
    private _sessionContext: SessionContext) {
    super(messageService);
  }

  public getStatuses(): Observable<Status[]> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/notificationStatuses/';
    this.createCommonHeaders(headers, this._sessionContext);

    return this._http.get(url, { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch((error) => { return super.handleErrorPromise(error); });
  }

  public getPriorities(): Observable<Priority[]> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/notificationPriorities/';
    this.createCommonHeaders(headers, this._sessionContext);

    return this._http.get(url, { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch((error) => { return super.handleErrorPromise(error); });
  }

  public getBranches(): Observable<Branch[]> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/branches/';
    this.createCommonHeaders(headers, this._sessionContext);

    return this._http.get(url, { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch((error) => { return super.handleErrorPromise(error); });
  }

  public getGridTerritories(): Observable<GridTerritory[]> {
    const headers = new Headers();
    const url = super.getBaseUrl() + '/gridTerritories/';
    this.createCommonHeaders(headers, this._sessionContext);

    return this._http.get(url, { headers: headers })
      .map(res => super.extractData(res, this._sessionContext))
      .catch((error) => { return super.handleErrorPromise(error); });
  }

}

