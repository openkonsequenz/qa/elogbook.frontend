/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { ImportService } from './import.service';
import { MessageService } from './message.service';
import { NotificationFileModel } from '../model/file-model';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';

describe('Http-ImportService (mockBackend)', () => {

  let sessionContext: SessionContext;
  const messageService: MessageService = new MessageService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        ImportService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext,
        MessageService
      ]
    })
      .compileComponents();
    sessionContext = new SessionContext();
  }));

  it('can instantiate service when inject service',
    inject([ImportService], (service: ImportService) => {
      expect(service instanceof ImportService).toBe(true);
    }));

  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new ImportService(http, sessionContext, messageService);
    expect(service instanceof ImportService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
    }));



});