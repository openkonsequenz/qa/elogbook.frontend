/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { NotificationService } from './notification.service';
import { Notification } from '../model/notification';
import { CURRENT_NOTIFICATIONS, FUTURE_NOTIFICATIONS } from '../test-data/notifications';
import { OPEN_NOTIFICATIONS, FINISHED_NOTIFICATIONS, DUMMY_NOTIFICATION } from '../test-data/notifications';
import { DUMMY_CREATED_NOTIFICATION, DUMMY_UPDATED_NOTIFICATION } from '../test-data/notifications';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { MessageService } from '../services/message.service';

describe('Http-NotificationService (mockBackend)', () => {
  let messageService: MessageService;
  let sessionContext: SessionContext;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    messageService = new MessageService();
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        { provide: MessageService, useValue: messageService }, 
        NotificationService,
        { provide: XHRBackend, useClass: MockBackend },
       
        SessionContext,
        MessageService
      ]
    })
      .compileComponents();
    sessionContext = new SessionContext();
   
  }));

  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new NotificationService(http, sessionContext, messageService);
    expect(service instanceof NotificationService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
    }));

  describe('when getNotifications()', () => {
    let backend: MockBackend;
    let service: NotificationService;
    let response: Response;
    let createNotificationResponse: Response;
    let udpateNotificationResponse: Response;
    const fakeResps: Notification[] = CURRENT_NOTIFICATIONS;
    const fakeCreateNotificationResps: Notification = DUMMY_CREATED_NOTIFICATION;
    const fakeUpdateNotificationResps: Notification = DUMMY_UPDATED_NOTIFICATION;
    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new NotificationService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const updateNotificationOptions = new ResponseOptions({ status: 200, body: fakeUpdateNotificationResps, headers: successHeaders });
      const createNotificationOptions = new ResponseOptions({ status: 200, body: fakeCreateNotificationResps, headers: successHeaders });
      const options = new ResponseOptions({ status: 200, body: fakeResps, headers: successHeaders });
      response = new Response(options);
      createNotificationResponse = new Response(createNotificationOptions);
      udpateNotificationResponse = new Response(updateNotificationOptions);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));

    it('create call should response a fake notification with new id', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(createNotificationResponse));

      service.itemAdded$.subscribe(item => expect(item.id).toBe(DUMMY_CREATED_NOTIFICATION.id));
      service.createNotification(DUMMY_NOTIFICATION).toPromise()
        .then(notification => {
          expect(notification.id).toBe(DUMMY_CREATED_NOTIFICATION.id);
        });
    })));

    it('update call should response a fake notification with new id', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(udpateNotificationResponse));

      service.itemAdded$.subscribe(item => expect(item.version).toBe(DUMMY_UPDATED_NOTIFICATION.version));
      service.updateNotification(DUMMY_CREATED_NOTIFICATION).toPromise()
        .then(notification => {
          expect(notification.version).toBe(DUMMY_UPDATED_NOTIFICATION.version);
        });
    })));

    it('should have expected fake notifications (then)', async(inject([], () => {

      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getNotifications('dummy').toPromise()
        .then(notifications => {
          expect(notifications.length).toBe(2);
          expect(notifications[0]).not.toBeNull();
          expect(notifications[1]).not.toBeNull();
          expect(notifications[0].id).toBe(100);
          expect(notifications[1].id).toBe(101);
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.getNotifications('dummy')
        .do(notifications => {
          fail('should not respond with notifications');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));

    it('should call the correct getNotificationFunction', () => {
      class MockNotificationService extends NotificationService {
        lastCall: String = '';

        getNotifications(type: string): Observable<Notification[]> {
          this.lastCall = type;
          return null;
        }
      }

      const serviceMock = new MockNotificationService(null, null, messageService);
      serviceMock.getAllNotifications();
      expect(serviceMock.lastCall).toBe(NotificationService.ALL);
      serviceMock.getCurrentNotifications();
      expect(serviceMock.lastCall).toBe(NotificationService.CURRENT);
      serviceMock.getFutureNotifications(null);
      expect(serviceMock.lastCall).toBe(NotificationService.FUTURE);
      serviceMock.getOpenNotifications(null);
      expect(serviceMock.lastCall).toBe(NotificationService.OPEN);
      serviceMock.getFinishedNotifications(null);
      expect(serviceMock.lastCall).toBe(NotificationService.FINISHED);
    });
  });

  describe('when getNotification()', () => {
    let backend: MockBackend;
    let service: NotificationService;
    let response: Response;
    const fakeNotifications: Notification[] = CURRENT_NOTIFICATIONS;
    const fakeNot = fakeNotifications[0];
    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new NotificationService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({ status: 200, body: fakeNot, headers: successHeaders });
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));

    it('should have expected fake notification (then)', async(inject([], () => {

      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getNotification(1).toPromise()
        .then(notification => {
          expect(notification).not.toBeNull();
          expect(notification.id).toBe(100);
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.getNotification(1)
        .do(notification => {
          fail('should not respond with notification');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));

    it('should call the correct getNotificationFunction', () => {
      class MockNotificationService extends NotificationService {
        lastCall: String = '';

        getNotifications(type: string): Observable<Notification[]> {
          this.lastCall = type;
          return null;
        }
      }

      const serviceMock = new MockNotificationService(null, null, messageService);
      serviceMock.getAllNotifications();
      expect(serviceMock.lastCall).toBe(NotificationService.ALL);
      serviceMock.getCurrentNotifications();
      expect(serviceMock.lastCall).toBe(NotificationService.CURRENT);
      serviceMock.getFutureNotifications(null);
      expect(serviceMock.lastCall).toBe(NotificationService.FUTURE);
      serviceMock.getOpenNotifications(null);
      expect(serviceMock.lastCall).toBe(NotificationService.OPEN);
      serviceMock.getFinishedNotifications(null);
      expect(serviceMock.lastCall).toBe(NotificationService.FINISHED);
    });
  });

  describe('when getNotificationVersions()', () => {
    let backend: MockBackend;
    let service: NotificationService;
    let response: Response;
    const fakeNotifications: Notification[] = CURRENT_NOTIFICATIONS;
    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new NotificationService(http, sessionContext, messageService);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({ status: 200, body: fakeNotifications, headers: successHeaders });
      response = new Response(options);
      sessionContext.setCurrSessionId(''); // current SessionID in LocalStorage is empty
    }));

    it('should have expected fake notification (then)', async(inject([], () => {

      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

      service.getNotificationVersions(1).toPromise()
        .then(notifications => {
          expect(notifications).not.toBeNull();
          expect(notifications.length).toBe(2);
          expect(notifications[0].id).toBe(100);
          expect(notifications[1].id).toBe(101);
          expect(sessionContext.getCurrSessionId()).not.toBe('SuperVALID!'); // only done with authentification
        });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
      const resp = new Response(new ResponseOptions({ status: 404 }));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

      service.getNotificationVersions(1)
        .do(notification => {
          fail('should not respond with notification');
        })
        .catch(err => {
          const str = err;
          expect(err).toMatch('Bad response status', 'should catch bad response status code');
          return Observable.of(null); // failure is the expected test result
        })
        .toPromise();
    })));
  });

});
