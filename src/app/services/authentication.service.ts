/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { SessionContext } from '../common/session-context';
import { BaseHttpService } from './base-http.service';
import { User } from '../model/user';
import { LoginCredentials } from '../model/login-credentials';

import { MessageService } from './message.service';

@Injectable()
export class AuthenticationService extends BaseHttpService {

    constructor(
        private _http: Http,
        public messageService: MessageService,
        private _sessionContext: SessionContext) {
        super(messageService);
    }

    public logout(): Observable<any> {
        const headers = new Headers();
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(super.getBaseUrl() + '/logout', { headers: headers })
            .map(res => {
                super.extractSessionId(res.headers, this._sessionContext);
                return super.extractData(res, this._sessionContext);
            })
            .catch((error) => {
                return super.handleErrorPromise(error);
            });
    }
}
