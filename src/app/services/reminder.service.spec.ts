/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { ReminderService } from './reminder.service';
import { NotificationService } from './notification.service';
import { Notification } from '../model/notification';
import { CURRENT_NOTIFICATIONS, FUTURE_NOTIFICATIONS } from '../test-data/notifications';
import { OPEN_NOTIFICATIONS, FINISHED_NOTIFICATIONS, DUMMY_NOTIFICATION } from '../test-data/notifications';
import { DUMMY_CREATED_NOTIFICATION, DUMMY_UPDATED_NOTIFICATION } from '../test-data/notifications';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { MessageService } from '../services/message.service';

describe('Http-ReminderService (mockBackend)', () => {

  let sessionContext: SessionContext;
  let messageService: MessageService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        ReminderService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext,
        MessageService
      ]
    })
      .compileComponents();
    messageService = new MessageService();
    sessionContext = new SessionContext();
  }));

  it('can instantiate service when inject service',
    inject([ReminderService], (service: ReminderService) => {
      expect(service instanceof ReminderService).toBe(true);
    }));

  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new ReminderService(http, sessionContext, messageService);
    expect(service instanceof ReminderService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
    }));


});
