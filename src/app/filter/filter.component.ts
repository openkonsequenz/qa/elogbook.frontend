/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, Optional, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Responsibility } from '../model/responsibility';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { User } from '../model/user';
import { MessageService, MessageDefines } from '../services/message.service';
import { ResponsibilityComponent } from '../dialogs/responsibility/responsibility.component';
import { ResponsibilityService } from '../services/responsibility.service';
import { RESPONSIBILITIES } from '../test-data/responsibilities';
import { BannerMessage } from '../common/banner-message';
import { BannerMessageStatusEn, ErrorType } from '../common/enums';
import { SessionContext } from '../common/session-context';
import { Notification } from '../model/notification';
import { FilterSelection } from '../model/filter-selection';
import { TerritoryBranchContainer } from '../model/territory-branch-container';
import { FilterMatrix } from '../model/controller-model/filter-matrix';
import { UserService } from 'app/services/user.service';
import { UserSettings } from 'app/model/user-settings';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnChanges {

  user: User = null;
  bannerMessage: BannerMessage = new BannerMessage();
  bannerMessageStatus = BannerMessageStatusEn;

  @Input() shiftChangeProtocolConfirmed: boolean;
  @Input() shiftChangeProtocolOpened: boolean;
  @Input() filterExpanded: boolean;

  responsibilityContainerMatrix: TerritoryResponsibility[] = [];
  filterSelection: FilterSelection = new FilterSelection();
  filterMatrix: FilterMatrix = null;

  constructor(
    public sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService,
    private messageService: MessageService,
    private userService: UserService
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['shiftChangeProtocolConfirmed'] !== undefined
      && changes['shiftChangeProtocolConfirmed'].currentValue !== undefined) {
      this.getSessionFilterMatrix();
    }

    if (changes['shiftChangeProtocolOpened'] !== undefined
      && changes['shiftChangeProtocolOpened'].currentValue !== undefined) {
      if (!changes['shiftChangeProtocolOpened'].currentValue) {
        this.initFilterMatrixWithDefaults();
      }
    }
  }

  ngOnInit() {
    this.user = this.sessionContext.getCurrUser();
    this.messageService.respConfirmed$.subscribe( dummy => this.onRespForCurrentUserChanged());
  }


  getResponsiblity(responsibilityContainer: TerritoryResponsibility, branchName: string): Responsibility {    
    return responsibilityContainer.
      responsibilityList ? responsibilityContainer.responsibilityList.
        find(responsibility => responsibility.branchName === branchName) : null;
  }

  onRespForCurrentUserChanged() {
    this.getSessionFilterMatrix();
    this.filterExpanded = true;
  }

  initFilterMatrixWithDefaults() {    
    this.filterMatrix = this.sessionContext.getfilterMatrix();
    if (!this.filterMatrix) {
      const userSettings: UserSettings = this.sessionContext.getUsersSettings();

      this.responsibilityService.getAllResponsibilities().subscribe(resps => {

        this.responsibilityContainerMatrix = resps;
        for (const responsibilityContainer of this.responsibilityContainerMatrix) {
          for (const responsibility of responsibilityContainer.responsibilityList) {            
            if (userSettings && userSettings.activeFilterRespIds) {              
              responsibility.isActive = userSettings.activeFilterRespIds.indexOf(responsibility.id)>-1;
            } else {
              //If no there is no custom user setting, default value will be used
              responsibility.isActive = responsibility.responsibleUser === this.user.username;
            }            
          }
        }      
        this.responsibilitiesSelectionChanged(false);
      },
        error => {
          this.messageService.emitError('Filtermatrix', ErrorType.retrieve);
        });
    }
  }

  getSessionFilterMatrix() {
    this.filterMatrix = this.sessionContext.getfilterMatrix();
    if (this.filterMatrix) {
      this.getUpdatedResponsibilities();
    }
  }

  getUpdatedResponsibilities() {
    const userSettings: UserSettings = this.sessionContext.getUsersSettings();    
    let updatedMatrix: TerritoryResponsibility[] = [];
    this.responsibilityService.getAllResponsibilities().subscribe(resps => {

      updatedMatrix = resps;
      for (const updatedContainer of updatedMatrix) {
        for (const updatedResp of updatedContainer.responsibilityList) {                      
            if (userSettings && userSettings.activeFilterRespIds) {              
              updatedResp.isActive = userSettings.activeFilterRespIds.indexOf(updatedResp.id)>-1;
            } else {
              //If no there is no custom user setting, default value will be used
              updatedResp.isActive = updatedResp.responsibleUser === this.user.username;
            } 
        }
      }

      this.responsibilityContainerMatrix = updatedMatrix;
      const filterMatrix: FilterMatrix = new FilterMatrix(updatedMatrix);
      this.messageService.matrixFilterChanged$.emit(filterMatrix.getNumFilterList());
    },
      error => {
        this.messageService.emitError('Filtermatrix', ErrorType.retrieve);
      }
    );
  }

  selectAllResponsibilities() {
    for (const responsibilityContainer of this.responsibilityContainerMatrix) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        responsibility.isActive = true;
      }
    }
    this.responsibilitiesSelectionChanged(true);
  }

  deselectAllResponsibilities() {
    for (const responsibilityContainer of this.responsibilityContainerMatrix) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        responsibility.isActive = false;
      }
    }
    this.responsibilitiesSelectionChanged(true);
  }

  resetToDefaultAllResponsibilities() {         
    for (const responsibilityContainer of this.responsibilityContainerMatrix) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        responsibility.isActive = responsibility.responsibleUser === this.user.username;
      }
    }  
    this.responsibilitiesSelectionChanged(false);
    this.resetUserSettingsToDefault();   
  }
  
  responsibilitiesSelectionChanged(updateUserSettings: boolean) {
    const filterMatrix: FilterMatrix = new FilterMatrix(this.responsibilityContainerMatrix);
    this.sessionContext.setfilterMatrix(filterMatrix);
    
    if (updateUserSettings) {
      this.updateUserSettingsResponsibilities(filterMatrix.getNumFilterList());
    }
    
    this.messageService.matrixFilterChanged$.emit(filterMatrix.getNumFilterList());
  }

  mapUserName( shortUsr: string ) {
    const userMap = this.sessionContext.getUserMap();
    return userMap ? this.sessionContext.getUserMap().findAndRenderUser(shortUsr) : shortUsr;
  }

  updateUserSettingsResponsibilities(numFilterList: number[]){
    const userSettings: UserSettings = this.sessionContext.getUsersSettings();    
    userSettings.activeFilterRespIds = numFilterList;
    this.updateUserSettings(userSettings);
  }

  updateUserSettings(userSettings: UserSettings){      
    this.sessionContext.setUsersSettings(userSettings);
    this.userService.postUserSettings(userSettings).subscribe(resp => {                  
    });
  }

  resetUserSettingsToDefault(){
    const userSettings: UserSettings = new UserSettings();
    this.updateUserSettings(userSettings);
  }



}
