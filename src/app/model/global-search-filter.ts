/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
export class GlobalSearchFilter {
    searchString: string;
    responsibilityForwarding: string;

    statusOpenSelection: boolean;
    statusInWorkSelection: boolean;
    statusDoneSelection: boolean;
    statusClosedSelection: boolean;

    fkRefBranch: number;
    fkRefGridTerritory: number;
    fastSearchSelected: boolean;
    fkRefNotificationPriority: number;
}