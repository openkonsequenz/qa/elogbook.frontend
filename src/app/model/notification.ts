/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Globals } from "app/common/globals";
export class Notification {
    id: number;
    selected: boolean;
    incidentId: number;
    fkRefNotificationPriority: number;
    status: string;
    fkRefBranch: number;
    fkRefGridTerritory: number;
    fkRefNotificationStatus: number;
    beginDate: string;
    notificationText: string;
    freeText: string;
    freeTextExtended: string;
    responsibilityForwarding: string;
    responsibilityForwardingUuid: string;
    responsibilityControlPoint: string;
    reminderDate: string;
    futureDate: string;
    expectedFinishDate: string;
    finishedDate: string;
    createDate: string;
    modDate: string;
    modUser: string;
    createUser: string;
    version: number;
    clerk: string;
    adminFlag: boolean;
    type: string;

    decoratorNotificationVersions?: Notification[];
    historyOpen?: boolean;

    public isTypePresenceCheck() {
        if(this.type && this.type === Globals.NOTIFICATION_TYPE_PRESENCE_CHECK) {
            return true;
        }
        return false;
    }
}


