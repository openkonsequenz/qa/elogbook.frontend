/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { TerritoryResponsibility } from '../../model/territory-responsibility';

export class FilterMatrix {
    responsibilityContainerMatrix: TerritoryResponsibility[] = [];
    
    
    constructor(responsibilityContainerMatrix?: TerritoryResponsibility[]) {
        this.responsibilityContainerMatrix = responsibilityContainerMatrix;
    }    

    public getNumFilterList(): number[] {
        const filterList: number[] = [];

        for (const responsibilityContainer of this.responsibilityContainerMatrix) {
            for (const responsibility of responsibilityContainer.responsibilityList) {
                if (responsibility.isActive) {
                    filterList.push(responsibility.id);
                }
            }
        }
        return filterList;
    }
}


