/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
export class FilterSelection {
    all = true;
    status = false;
    notificationText = false;
    freeText = false;
    freeTextExtended = false;
    responsibilityForwarding = false;
    responsibilityControlPoint = false;
    creator = false;
    clerk = false;

    resetFilterSelection(): void {
        this.all = true;
        this.status = false;
        this.notificationText = false;
        this.freeText = false;
        this.freeTextExtended = false;
        this.responsibilityForwarding = false;
        this.responsibilityControlPoint = false;
        this.creator = false;
        this.clerk = false;
    }
}
