/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { HistoricalResponsibility } from './historical-responsibility';

export class HistoricalShiftChanges {
    
    /** begin of the specified period */
    startDate: string;

    /** end of the specified period */
    endDate: string;
 
    /** Collection of historical shift change documentation data. */ 
    historicalResponsibilities: HistoricalResponsibility[];  

}
