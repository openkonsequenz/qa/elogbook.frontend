/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Branch } from './branch';
import { GridTerritory } from './gridterritory';

export class HistoricalResponsibility {
    id: number;
    responsibleUser: string;
    formerResponsibleUser: string;
    transferDate: string;
    transactionId: number;
    createDate: string;
    createUser: string;
    modDate: string;
    modUser: string;
    refGridTerritory: GridTerritory;
    RefBranch: Branch;

    public constructor(init?: Partial<HistoricalResponsibility>) {
        Object.assign(this, init);
    }
}
