// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

//process.env.CHROME_BIN = require('puppeteer').executablePath()

module.exports = function (config) {
  config.set({    
    browserDisconnectTolerance: 3,
    browserNoActivityTimeout: 30000,
	browserDisconnectTimeout: 10000,    
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-junit-reporter'),
      require('@angular/cli/plugins/karma')      
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    files: [{
        pattern: './src/test.ts',
        watched: false
      }
      //{ pattern: './node_modules/bootstrap/dist/js/bootstrap.min.js', included: false, watched: false },
      //{ pattern: './node_modules/jquery/dist/jquery.min.js', included: false, watched: false }
    ],
    preprocessors: {
      './src/test.ts': ['@angular/cli']
    },
    mime: {
      'text/x-typescript': ['ts', 'tsx']
    },
    coverageIstanbulReporter: {
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: config.angularCli && config.angularCli.codeCoverage ?
      ['progress', 'coverage-istanbul'] :
      ['progress', 'kjhtml', 'dots', 'junit'],
    junitReporter: {
      outputDir: 'unittests',
      outputFile: 'test-results.xml'
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_DEBUG,
    autoWatch: true,
    browsers: ['ChromeDebugging'],
    customLaunchers: {
      ChromeDebugging: {
        // base: 'ChromeHeadless',
        base: 'Chrome',
        flags: ['--remote-debugging-port=9333']
      }
    },
    singleRun: false
  });
};
